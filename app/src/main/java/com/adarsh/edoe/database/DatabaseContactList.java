package com.adarsh.edoe.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.adarsh.edoe.handler.ContactHandler;

import java.util.ArrayList;
import java.util.List;

public class DatabaseContactList extends SQLiteOpenHelper {

    // DatabaseRegistrationStudent Version
    private static final int DATABASE_VERSION = 2;

    // DatabaseRegistrationStudent Name
    private static final String DATABASE_NAME = "DB_Contact";

    // Contacts table name
    private static final String TABLE_CONTACT = "user_contact_list";
    // Contacts Table Columns names
    private static final String KEY_ID = "_id";
    private static final String CONTACT_NAME = "question_type";
    private static final String CONTACT_NUMBER = "question";


    ArrayList test = new ArrayList();


    public DatabaseContactList(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_CONTACT
                + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
                + CONTACT_NAME + " TEXT NOT NULL, "
                + CONTACT_NUMBER + " TEXT NOT NULL " + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);

        // Create tables again
        onCreate(db);
    }

    public boolean addContentQues(String CONTACT_NAMEContent, String CONTACT_NUMBERContent) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CONTACT_NAME, CONTACT_NAMEContent);
        values.put(CONTACT_NUMBER, CONTACT_NUMBERContent);

        boolean added2DB = false;

        long rowInserted = db.insert(TABLE_CONTACT, null, values);
        if (rowInserted != -1) {
            added2DB = true;
        } else {
            added2DB = false;
        }

        db.close(); // Closing database connection
        return added2DB;
    }

    public String getContent1(String search) {
        SQLiteDatabase db = this.getReadableDatabase();
        String FirstNameString = "";
        Cursor cursor = null;
        try {
            cursor = db.query(TABLE_CONTACT, null, CONTACT_NUMBER + " =?", new String[]{search}, null, null, null);
        } catch (Exception r) {
            r.printStackTrace();
        }

        int CONTACT_NAMEInt = cursor.getColumnIndex(CONTACT_NAME);

        cursor.moveToFirst();

        try {
            FirstNameString = cursor.getString(CONTACT_NAMEInt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FirstNameString;
    }

    //     Getting All Contacts
    public List<ContactHandler> getAllContents() {
        List<ContactHandler> contactList = new ArrayList<ContactHandler>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        int IDInt = cursor.getColumnIndex(KEY_ID);
        int CONTACT_NAMEnt = cursor.getColumnIndex(CONTACT_NAME);
        int CONTACT_NUMBERInt = cursor.getColumnIndex(CONTACT_NUMBER);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ContactHandler contact = new ContactHandler();
                contact.setID(Integer.parseInt(cursor.getString(IDInt)));
                contact.setCname(cursor.getString(CONTACT_NAMEnt));
                contact.setCnumber(cursor.getString(CONTACT_NUMBERInt));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        // return contact list
        return contactList;
    }
}

