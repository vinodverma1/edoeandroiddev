package com.adarsh.edoe.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CameraPreview;
import com.adarsh.edoe.app_helper.CheckAppPermission;

public class CameraActivity extends AppCompatActivity {

    int maxFlash = 0, minFlash = 0;
    boolean CheckForFlash = false;
    ImageView backArrow;
    Button ManualUploadBtn;
    private Camera mCamera;
    private CameraPreview mPreview;

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        backArrow = (ImageView) findViewById(R.id.back_arrow_camera_activity);
        ManualUploadBtn = (Button) findViewById(R.id.manualUploadBtn_camera_activity);

        boolean check = checkCameraHardware(this);

        if (check) {
            // Create an instance of Camera
            mCamera = getCameraInstance();

            CheckForFlash = CameraActivity.this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
            if (CheckForFlash) {
                try {
                   /*
                   if (ContextCompat.checkSelfPermission(CameraActivity.this,
                           Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                           ContextCompat.checkSelfPermission(CameraActivity.this,
                                   Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                       //Request for permission for external storage

                       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                           requestPermissions(
                                   new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                   100);
                       }else{
                           ActivityCompat.requestPermissions(CameraActivity.this,
                                   new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                   100);
                       }

                   } else {
                       Camera.Parameters parametro = mCamera.getParameters();
                       parametro.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                       mCamera.setParameters(parametro);
                       mCamera.startPreview();
                   } */

                    final CheckAppPermission checkAppPermission = new CheckAppPermission(CameraActivity.this);
                    if (Build.VERSION.SDK_INT >= 23 && !checkAppPermission.checkPermissionForCamera()) {
                        checkAppPermission.requestPermissionForCamera();
                    } else {
                        Camera.Parameters parametro = mCamera.getParameters();
                        parametro.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        mCamera.setParameters(parametro);
                        mCamera.startPreview();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(this, "Your Phone Does Not Support Camera", Toast.LENGTH_SHORT).show();
        }

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraActivity.this.finish();
            }
        });

        ManualUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cam = new Intent(CameraActivity.this, AddCardManual.class);
                startActivity(cam);
            }
        });
    }

    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
}
