package com.adarsh.edoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.adarsh.edoe.R;

public class AddPaymentCard extends Activity {

    Button AddBank, AddDebitCard;
    ImageView backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_payment_card);

        AddBank = (Button) findViewById(R.id.add_bank_account);
        AddDebitCard = (Button) findViewById(R.id.add_debit_account);
        backArrow = (ImageView) findViewById(R.id.back_arrow_add_payment_card);

        AddBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hhh = new Intent(AddPaymentCard.this, BankLists.class);
                startActivity(hhh);
            }
        });

        AddDebitCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hhh = new Intent(AddPaymentCard.this, CameraActivity.class);
                startActivity(hhh);
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddPaymentCard.this.finish();
            }
        });
    }
}
