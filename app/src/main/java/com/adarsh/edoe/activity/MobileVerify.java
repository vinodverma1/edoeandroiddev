package com.adarsh.edoe.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;

public class MobileVerify extends Activity {

    EditText MobileED2;
    TextView MobileED1;
    boolean check = false;
    String Mobile = "", type = "", CheckSting = "";
    CheckInternet checkInternet;
    Button NextBtn;
    ImageView back_arrow_mobileVerify;
    LinearLayout OneBtn, TwoBtn, ThreeBtn, FourBtn, FiveBtn, SixBtn, SevenBtn, EightBtn, NineBtn, ZeroBtn, DeleteBtn;
    TextWatcher watch = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            try {
                if (s.length() == 5) {
                    if (check) {

                    } else {
                        String ddd = MobileED1.getText().toString();
                        ddd = ddd + " ";
                        CheckSting = ddd;
                        MobileED1.setText(ddd);
                        NextBtn.setBackgroundColor(getResources().getColor(R.color.grey));
//                MobileED1.setSelection(MobileED1.getText().length());
                    }
                } else if (s.length() == 12) {
                    NextBtn.setBackgroundColor(getResources().getColor(R.color.btn_color));
                } else if (s.length() == 6) {
                    if (check) {
                        NextBtn.setBackgroundColor(getResources().getColor(R.color.grey));
                        String ddd = MobileED1.getText().toString();
                        try {

                            ddd = ddd.substring(0, ddd.length() - 1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        check = false;
                        MobileED1.setText(ddd);

                    } else {

                    }
//             MobileED1.setSelection(MobileED1.getText().length());
                } else if (s.length() < 12) {
                    NextBtn.setBackgroundColor(getResources().getColor(R.color.grey));
//                MobileED1.setSelection(MobileED1.getText().length());
                } else if (s.length() == 0) {
                    NextBtn.setBackgroundColor(getResources().getColor(R.color.grey));
//                MobileED1.setSelection(MobileED1.getText().length());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_verify);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        checkInternet = new CheckInternet(this);

        MobileED1 = (TextView) findViewById(R.id.edt_mobile_mobile_verify);
        // MobileED2=(EditText)findViewById(R.id.edt_mobile_mobile_verify2);
        NextBtn = (Button) findViewById(R.id.nextBtn_mobile_verify);
        back_arrow_mobileVerify = (ImageView) findViewById(R.id.back_arrow_mobileVerify1);

        try {
            OneBtn = (LinearLayout) findViewById(R.id.oneBtn);
            TwoBtn = (LinearLayout) findViewById(R.id.twoBtn);
            ThreeBtn = (LinearLayout) findViewById(R.id.threeBtn);
            FourBtn = (LinearLayout) findViewById(R.id.fourBtn);
            FiveBtn = (LinearLayout) findViewById(R.id.fiveBtn);
            SixBtn = (LinearLayout) findViewById(R.id.sixBtn);
            SevenBtn = (LinearLayout) findViewById(R.id.sevenBtn);
            EightBtn = (LinearLayout) findViewById(R.id.eightBtn);
            NineBtn = (LinearLayout) findViewById(R.id.nineBtn);
            ZeroBtn = (LinearLayout) findViewById(R.id.zeroBtn);
            DeleteBtn = (LinearLayout) findViewById(R.id.deleteBtn);


            OneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "1";
                    MobileED1.setText(Mobile);

//                    MobileED1.setSelection(MobileED1.getText().length());
                }
            });

            TwoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "2";
                    MobileED1.setText(Mobile);

//                    MobileED1.setSelection(MobileED1.getText().length());
                }
            });

            ThreeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "3";
                    MobileED1.setText(Mobile);

//                    MobileED1.setSelection(MobileED1.getText().length());
                }
            });

            FourBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "4";
                    MobileED1.setText(Mobile);

//                    MobileED1.setSelection(MobileED1.getText().length());
                }
            });

            FiveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "5";
                    MobileED1.setText(Mobile);
//                    MobileED1.setSelection(MobileED1.getText().length());
                }
            });

            SixBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "6";
                    MobileED1.setText(Mobile);
//                    MobileED1.setSelection(MobileED1.getText().length());
                }
            });

            SevenBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "7";
                    MobileED1.setText(Mobile);
//                    MobileED1.setSelection(MobileED1.getText().length());
                }
            });

            EightBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "8";
                    MobileED1.setText(Mobile);

                }
            });

            NineBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "9";
                    MobileED1.setText(Mobile);

                }
            });

            ZeroBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check = false;
                    Mobile = MobileED1.getText().toString() + "0";
                    MobileED1.setText(Mobile);
                }
            });

            DeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String cc = MobileED1.getText().toString().substring(MobileED1.getText().length() - 1);
                        if (cc.equalsIgnoreCase(" ")) {
                            check = true;
                            Mobile = MobileED1.getText().toString();
                            int acd = Mobile.length();
                            Mobile = Mobile.substring(0, acd - 2);
                            MobileED1.setText(Mobile);
                        } else {
                            check = true;
                            Mobile = MobileED1.getText().toString();
                            int acd = Mobile.length();
                            Mobile = Mobile.substring(0, acd - 1);
                            MobileED1.setText(Mobile);
//                    MobileED1.setSelection(MobileED1.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            type = getIntent().getStringExtra("Type");
        } catch (Exception e) {
            e.printStackTrace();
        }

        back_arrow_mobileVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobileVerify.this.finish();
            }
        });


        NextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Mobile = MobileED1.getText().toString().trim();
                } catch (Exception e) {
                    Mobile = "";
                    e.printStackTrace();
                }

                if (TextUtils.isEmpty(Mobile)) {
                    Toast.makeText(MobileVerify.this, "Please enter your mobile number", Toast.LENGTH_SHORT).show();
                } else {
                    int abcd = Mobile.length();
                    if (abcd == 12) {

                        Intent otpIntent = new Intent(MobileVerify.this, OtpVerify.class);
                        otpIntent.putExtra("Type", type);
                        otpIntent.putExtra("Mobile", Mobile);
                        startActivity(otpIntent);
                        MobileVerify.this.finish();
                    } else {
                        Toast.makeText(MobileVerify.this, "Enter your correct mobile number", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        try {
            MobileED1.addTextChangedListener(watch);
            //  MobileED2.addTextChangedListener(watch1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
