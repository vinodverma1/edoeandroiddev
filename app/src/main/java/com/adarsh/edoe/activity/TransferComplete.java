package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;

public class TransferComplete extends AppCompatActivity {

    SessionManager sessionManager;

    Button OkayBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transfer_complete);

        sessionManager = new SessionManager(this);

        OkayBtn = (Button) findViewById(R.id.okay_transferComplete);

        OkayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferComplete.this.finish();
            }
        });
    }
}
