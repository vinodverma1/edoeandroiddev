package com.adarsh.edoe.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;

public class NewCardAdded extends Activity {

    SessionManager sessionManager;
    CheckInternet checkInternet;

    Button OkBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_card_added);

        checkInternet = new CheckInternet(this);
        sessionManager = new SessionManager(this);

        OkBtn = (Button) findViewById(R.id.okBtn_new_card_added);

        OkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewCardAdded.this.finish();
            }
        });

    }
}