package com.adarsh.edoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;

public class OtpVerify extends Activity {

    TextView OtpED;
    TextView MobileTV;
    String Otp = "", Mobile = "", type = "";
    CheckInternet checkInternet;
    Button DoneBtn;
    ImageView back_arrow_otp;
    LinearLayout OneBtn, TwoBtn, ThreeBtn, FourBtn, FiveBtn, SixBtn, SevenBtn, EightBtn, NineBtn, ZeroBtn, DeleteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verify);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        checkInternet = new CheckInternet(this);

        OtpED = (TextView) findViewById(R.id.otp_otp_verify);
        MobileTV = (TextView) findViewById(R.id.mobile_otp_verify);
        DoneBtn = (Button) findViewById(R.id.verifyBtn_otp_verify);
        back_arrow_otp = (ImageView) findViewById(R.id.back_arrow_otp);
        try {
            type = getIntent().getStringExtra("Type");
            Mobile = getIntent().getStringExtra("Mobile");
        } catch (Exception e) {
            e.printStackTrace();
        }

        MobileTV.setText(Mobile);
        try {
            OneBtn = (LinearLayout) findViewById(R.id.oneBtn);
            TwoBtn = (LinearLayout) findViewById(R.id.twoBtn);
            ThreeBtn = (LinearLayout) findViewById(R.id.threeBtn);
            FourBtn = (LinearLayout) findViewById(R.id.fourBtn);
            FiveBtn = (LinearLayout) findViewById(R.id.fiveBtn);
            SixBtn = (LinearLayout) findViewById(R.id.sixBtn);
            SevenBtn = (LinearLayout) findViewById(R.id.sevenBtn);
            EightBtn = (LinearLayout) findViewById(R.id.eightBtn);
            NineBtn = (LinearLayout) findViewById(R.id.nineBtn);
            ZeroBtn = (LinearLayout) findViewById(R.id.zeroBtn);
            DeleteBtn = (LinearLayout) findViewById(R.id.deleteBtn);


            OneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "1";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            TwoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "2";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            ThreeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "3";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            FourBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "4";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            FiveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "5";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            SixBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "6";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            SevenBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "7";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            EightBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "8";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            NineBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "9";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            ZeroBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Otp = OtpED.getText().toString() + "0";
                    OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                }
            });

            DeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Otp = OtpED.getText().toString();
                        int acd = Otp.length();
                        Otp = Otp.substring(0, acd - 1);
                        OtpED.setText(Otp);
//                    OtpED.setSelection(OtpED.getText().length());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        DoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Otp = OtpED.getText().toString().trim();
                } catch (Exception e) {
                    Otp = "";
                    e.printStackTrace();
                }

                if (TextUtils.isEmpty(Otp)) {
                    Toast.makeText(OtpVerify.this, "Please enter your OTP", Toast.LENGTH_SHORT).show();
                } else {

                    if (Otp.length() == 5) {
                        if (type.equalsIgnoreCase("1")) {
                            Intent otpIntent = new Intent(OtpVerify.this, ForgetPassword.class);
                            startActivity(otpIntent);
                            OtpVerify.this.finish();
                        } else {
                            Intent otpIntent = new Intent(OtpVerify.this, Registration.class);
                            startActivity(otpIntent);
                            OtpVerify.this.finish();
                        }
                    } else {
                        Toast.makeText(OtpVerify.this, "Length of OTP is wrong. Please fill correct OTP", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        back_arrow_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OtpVerify.this.finish();
            }
        });
    }
}

