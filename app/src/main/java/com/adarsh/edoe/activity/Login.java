package com.adarsh.edoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class Login extends Activity {

    SessionManager sessionManager;
    CheckInternet checkInternet;

    ArrayList<String> LoginTypeList;
    EditText LoginIdED, LoginPasswordED;
    Button LoginBtn, RegistrationLink, FbLoginBtn1, FbLoginBtn2;
    LinearLayout FbLogin;
    TextView ForgetPassLink;
    String LoginId = "", LoginPassword = "", LoginType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkInternet = new CheckInternet(this);
        sessionManager = new SessionManager(this);

        LoginTypeList = new ArrayList<>();

        LoginIdED = (EditText) findViewById(R.id.edt_phone_login);
        LoginPasswordED = (EditText) findViewById(R.id.edt_passwrd_login);
        ForgetPassLink = (TextView) findViewById(R.id.forgetPasswordLink_login);
        LoginBtn = (Button) findViewById(R.id.btn_login);
        FbLogin = (LinearLayout) findViewById(R.id.btn_fb_login);
        FbLoginBtn1 = (Button) findViewById(R.id.btn_fb_login1);
        FbLoginBtn2 = (Button) findViewById(R.id.btn_fb_login2);
        RegistrationLink = (Button) findViewById(R.id.btn_register_activity_login);

        try {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ForgetPassLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin = new Intent(Login.this, MobileVerify.class);
                intentLogin.putExtra("Type", "1");
                startActivity(intentLogin);
            }
        });

        LoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // LoginMethod();
                Intent lg = new Intent(Login.this, NavigationMain.class);
                startActivity(lg);

            }
        });

        RegistrationLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentLogin = new Intent(Login.this, MobileVerify.class);
                intentLogin.putExtra("Type", "0");
                startActivity(intentLogin);
            }
        });

    }

    public void LoginMethod() {
        try {
            LoginId = LoginIdED.getText().toString().trim();
            LoginPassword = LoginPasswordED.getText().toString().trim();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(LoginId)) {
            Toast.makeText(this, "Please Enter Your Phone/Mobile Number", Toast.LENGTH_SHORT).show();
        } else {
            int abcd = LoginId.length();
            if (abcd == 11) {

                if (TextUtils.isEmpty(LoginPassword)) {
                    Toast.makeText(this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                } else {
                    if (checkInternet.isNetworkAvailable1() || checkInternet.isWifiConnected()) {
                        Intent lg = new Intent(Login.this, NavigationMain.class);
                        startActivity(lg);
                        Login.this.finish();
                    } else {
                        Toast.makeText(this, "Make sure that your phone is connected to mobile internet or wifi", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(this, "Please enter your correct phone/mobile number", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
