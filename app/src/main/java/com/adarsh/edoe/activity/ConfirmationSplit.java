package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.ConfirmationSplitAdapter;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class ConfirmationSplit extends AppCompatActivity {

    Button ConfirmBtn;
    RecyclerView ContactListView;
    TextView DateTV, AmountTV;
    ArrayList<String> ContactNamelist, ContactNumberlist;
    String Amount = "";
    ImageView BackArrow;

    ConfirmationSplitAdapter adapterContact;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmation_split);

        sessionManager = new SessionManager(this);

        BackArrow = (ImageView) findViewById(R.id.back_arrow_confirmation_split);
        ConfirmBtn = (Button) findViewById(R.id.confirmBtn_confirmation_split);
        DateTV = (TextView) findViewById(R.id.date_confirmation_split);
        AmountTV = (TextView) findViewById(R.id.amount_confirmation_split);

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();

        ContactNamelist.clear();
        ContactNumberlist.clear();
        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmationSplit.this.finish();
            }
        });
        try {
            ContactNamelist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNameList");
            ContactNumberlist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNumberList");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Amount = getIntent().getStringExtra("Amount");
            AmountTV.setText(Amount);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ConfirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent ded = new Intent(ConfirmationSplit.this, NavigationMain.class);
                    ded.putExtra("ShowPendingFragment", "1");
                    startActivity(ded);
//                    Fragment fragment=null;
//                    fragment = new PendingFragment();
//                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                    transaction.add(R.id.frame_layout, fragment);
//                    transaction.replace(R.id.frame_layout, fragment);
//                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        ContactListView = (RecyclerView) findViewById(R.id.contactList_confirmation_split);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        adapterContact = new ConfirmationSplitAdapter(ConfirmationSplit.this, ContactNamelist, ContactNumberlist);
        ContactListView.setAdapter(adapterContact);

    }
}
