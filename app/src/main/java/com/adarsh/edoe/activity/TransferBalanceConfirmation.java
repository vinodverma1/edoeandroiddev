package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;

public class TransferBalanceConfirmation extends AppCompatActivity {

    SessionManager sessionManager;

    Button ConfirmTransferBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transfer_balance_confirmation);

        sessionManager = new SessionManager(this);

        ConfirmTransferBtn = (Button) findViewById(R.id.transferBtn_transferBalanceConfirmation);

        ConfirmTransferBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tran = new Intent(TransferBalanceConfirmation.this, TransferComplete.class);
                startActivity(tran);
                TransferBalanceConfirmation.this.finish();
            }
        });

    }
}
