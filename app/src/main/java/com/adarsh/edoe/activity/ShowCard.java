package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;

public class ShowCard extends AppCompatActivity {

    SessionManager sessionManager;
    CheckInternet checkInternet;

    Button ReomveCardBtn;
    ImageView backArrow;
    TextView CardNumberTV, CardTypeTV, CardExpirationTV;
    String CardNumber = "", CardType = "", CardExpiration = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_card);

        checkInternet = new CheckInternet(this);
        sessionManager = new SessionManager(this);

        ReomveCardBtn = (Button) findViewById(R.id.removeCardBtn_show_card);
        backArrow = (ImageView) findViewById(R.id.back_arrow_show_card);

        CardNumberTV = (TextView) findViewById(R.id.card_number_show_card);
        CardTypeTV = (TextView) findViewById(R.id.card_type_show_card);
        CardExpirationTV = (TextView) findViewById(R.id.card_expiration_show_card);

        ReomveCardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ShowCard.this, "Coming Soon!", Toast.LENGTH_SHORT).show();
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowCard.this.finish();
            }
        });

    }
}

