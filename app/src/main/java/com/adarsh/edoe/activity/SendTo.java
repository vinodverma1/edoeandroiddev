package com.adarsh.edoe.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;
import com.adarsh.edoe.database.DatabaseContactList;

public class SendTo extends AppCompatActivity {

    Button CardBtn, ContactBtn, ConfirmBtn;
    TextView AmountTV, PrivateTextTv, ToolbarName;
    EditText RemarkED;
    Switch switchBtn;
    String Toolbar = "", Amount = "";
    ImageView BackArrow;
    LinearLayout linearLayout;

    SessionManager sessionManager;
    DatabaseContactList dbContactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_to);

        sessionManager = new SessionManager(this);
        dbContactList = new DatabaseContactList(this);

        switchBtn = (Switch) findViewById(R.id.on_off_switch_send_to);
        linearLayout = (LinearLayout) findViewById(R.id.linear_send_to);
        BackArrow = (ImageView) findViewById(R.id.back_arrow_send_to);
        CardBtn = (Button) findViewById(R.id.cardBtn_send_to);
        ContactBtn = (Button) findViewById(R.id.contactBtn_send_to);
        ConfirmBtn = (Button) findViewById(R.id.confirmBtn_send_to);

        RemarkED = (EditText) findViewById(R.id.comment_send_to);
        PrivateTextTv = (TextView) findViewById(R.id.privatetext_send_to);
        AmountTV = (TextView) findViewById(R.id.amount_send_to);
        ToolbarName = (TextView) findViewById(R.id.toolbar_name_send_to);

        try {
            Amount = getIntent().getStringExtra("Amount");
            AmountTV.setText(Amount);
            Toolbar = getIntent().getStringExtra("Toolbar");
            ToolbarName.setText(getIntent().getStringExtra("Toolbar"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Toolbar.startsWith("r") || Toolbar.startsWith("R")) {
            CardBtn.setVisibility(View.GONE);
            RemarkED.setHint("Enter a description");
        } else {
            CardBtn.setVisibility(View.VISIBLE);
            RemarkED.setHint("Thank You!");
        }

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemarkED.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        });

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendTo.this.finish();
            }
        });

        ConfirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent confirm = null;
                String abc = "";
                try {
                    abc = Toolbar.substring(0, 2);
                    //Toast.makeText(SendTo.this, abc, Toast.LENGTH_SHORT).show();

                    if (abc.equalsIgnoreCase("Se")) {
                        confirm = new Intent(SendTo.this, ConfirmationSend.class);
                    } else if (abc.equalsIgnoreCase("Re")) {
                        confirm = new Intent(SendTo.this, ConfirmationRequest.class);
                    } else {

                    }
                    confirm.putExtra("Amount", Amount);
                    startActivity(confirm);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        switchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    PrivateTextTv.setText("Public");
                } else {
                    PrivateTextTv.setText("Private");
                }

            }
        });

        CardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }
}