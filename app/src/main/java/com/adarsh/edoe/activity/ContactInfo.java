package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;
import com.adarsh.edoe.fragment.BetweenYou;
import com.adarsh.edoe.fragment.Feeds;

import java.util.ArrayList;
import java.util.List;

public class ContactInfo extends AppCompatActivity {

    TextView NameToolbarTV, NumberToolbarTV;
    String NameToolbar = "", NumberToolbar = "";
    SessionManager sessionManager;
    ImageButton BackArrow;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_info);

        sessionManager = new SessionManager(this);

        NameToolbarTV = (TextView) findViewById(R.id.nameToolbar_contact_info);
        NumberToolbarTV = (TextView) findViewById(R.id.numberToolbar_contact_info);
        BackArrow = (ImageButton) findViewById(R.id.backBtn_contact_info);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        createViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();

        try {
            NameToolbar = getIntent().getStringExtra("ContactName");
            NumberToolbar = getIntent().getStringExtra("ContactNumber");
        } catch (Exception e) {
            e.printStackTrace();
        }
        NameToolbarTV.setText(NameToolbar);
        NumberToolbarTV.setText(NumberToolbar);

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactInfo.this.finish();
            }
        });
    }

    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Between You");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Feeds");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }

    private void createViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new BetweenYou(), "Between You");
        adapter.addFrag(new Feeds(), "Feeds");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
