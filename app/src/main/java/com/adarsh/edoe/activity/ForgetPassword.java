package com.adarsh.edoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;

public class ForgetPassword extends Activity {

    EditText PasswordED, ConfirmPasswordED;
    String Password = "", ConfirmPassword = "";
    CheckInternet checkInternet;
    Button DoneBtn;
    ImageView back_arrow_mobileVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_pasword);

        checkInternet = new CheckInternet(this);

        PasswordED = (EditText) findViewById(R.id.edt_password_forget_password);
        ConfirmPasswordED = (EditText) findViewById(R.id.edt_confirmpassword_forget_password);
        DoneBtn = (Button) findViewById(R.id.doneBtn_forget_password);

        back_arrow_mobileVerify = (ImageView) findViewById(R.id.back_arrow_forget_password);

        back_arrow_mobileVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgetPassword.this.finish();
            }
        });


        DoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PasswordChangeMethod();
            }
        });

    }

    public void PasswordChangeMethod() {

        try {
            try {
                Password = PasswordED.getText().toString().trim();
                ConfirmPassword = ConfirmPasswordED.getText().toString().trim();


            } catch (Exception e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(Password)) {
                Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
            } else {
                if (ConfirmPassword.equals(Password)) {
                    Toast.makeText(this, "Password changed. \nPlease login to continue.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    ForgetPassword.this.finish();
                } else {
                    Toast.makeText(this, "Both password are different. Please enter same password", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

