package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.PaidByAdapter;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class PaidBy extends AppCompatActivity {

    RecyclerView ContactListView;
    ImageView BackArrow;
    Button OkayBtn;
    ArrayList<String> ContactNamelist, ContactNumberlist;

    PaidByAdapter adapter;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paid_by);

        sessionManager = new SessionManager(this);

        OkayBtn = (Button) findViewById(R.id.okayBtn_paid_by);
        BackArrow = (ImageView) findViewById(R.id.back_arrow_paid_by);

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();

        ContactNamelist.clear();
        ContactNumberlist.clear();

        ContactListView = (RecyclerView) findViewById(R.id.contactList_paid_by);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        try {
            ContactNamelist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNameList");
            ContactNumberlist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNumberList");
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapter = new PaidByAdapter(PaidBy.this, ContactNamelist, ContactNumberlist);
        ContactListView.setAdapter(adapter);

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaidBy.this.finish();
            }
        });

        OkayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaidBy.this.finish();
            }
        });

    }
}
