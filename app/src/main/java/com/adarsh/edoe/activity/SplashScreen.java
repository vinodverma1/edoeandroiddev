package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    String a = "0";
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        sessionManager = new SessionManager(SplashScreen.this);

        try {
            a = sessionManager.GetLoginRemember();
        } catch (Exception e) {
            a = "0";
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (a.equals("1")) {
                    Intent i = new Intent(SplashScreen.this, Login.class);
                    startActivity(i);
                    SplashScreen.this.finish();
                } else {
                    Intent i = new Intent(SplashScreen.this, Login.class);
                    startActivity(i);
                    SplashScreen.this.finish();

                }
                // close this activity
                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}
