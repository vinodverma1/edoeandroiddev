package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.MeDetailAdapter;

import java.util.ArrayList;

public class MeDetail extends AppCompatActivity {
    RecyclerView recyclerview_me_detail;
    ArrayList<String> PostDate, PostLike, ActivityPost, ActivityMessage, ImageUrl;
    MeDetailAdapter adapter;
    ImageView back_arrow_me_detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.me_detail);

        //1120969129TPC090518121403
        //service id 1120969129
        //gateway id 513373938
        recyclerview_me_detail = (RecyclerView) findViewById(R.id.recyclerview_me_detail);
        back_arrow_me_detail = (ImageView) findViewById(R.id.back_arrow_me_detail);
        PostDate = new ArrayList<>();
        PostLike = new ArrayList<>();
        ActivityPost = new ArrayList<>();
        ActivityMessage = new ArrayList<>();
        ImageUrl = new ArrayList<>();
        PostDate.add("4d");
        PostDate.add("2d");

        PostLike.add("1 like");
        PostLike.add("2 like");

        ActivityPost.add("Steven Strange");
        ActivityPost.add("Tony Stark");

        ActivityMessage.add("YO! It was no problem.Let's do it again!");
        ActivityMessage.add("Yes Please! Let's do it again");
        adapter = new MeDetailAdapter(MeDetail.this, ImageUrl, ActivityPost, ActivityMessage, PostDate, PostLike);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(MeDetail.this, 1);

        recyclerview_me_detail.setLayoutManager(layoutManager);
        recyclerview_me_detail.setItemAnimator(new DefaultItemAnimator());
        //recyclerViewMe.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        recyclerview_me_detail.setAdapter(adapter);
        back_arrow_me_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeDetail.this.finish();
            }
        });
    }
}
