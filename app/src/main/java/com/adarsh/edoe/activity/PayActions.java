package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;

public class PayActions extends AppCompatActivity {

    TextView AmountTV;
    Button SednToBTn, RequestFromBtn, SplitBtn;
    ImageView BackArrow;
    String Amount = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_actions);

        AmountTV = (TextView) findViewById(R.id.amount_pay_actions);
        SednToBTn = (Button) findViewById(R.id.send_to_pay_actions);
        RequestFromBtn = (Button) findViewById(R.id.request_from_pay_actions);
        SplitBtn = (Button) findViewById(R.id.split_pay_actions);
        BackArrow = (ImageView) findViewById(R.id.back_arrow_pay_actions);

        try {
            Amount = getIntent().getStringExtra("amount");
        } catch (Exception e) {
            e.printStackTrace();
        }

        AmountTV.setText(Amount);

        SednToBTn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent action = new Intent(PayActions.this, PayActionPerform.class);
                action.putExtra("Amount", Amount);
                action.putExtra("Action", "Send To");
                startActivity(action);

            }
        });

        RequestFromBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent action = new Intent(PayActions.this, PayActionPerform.class);
                action.putExtra("Amount", Amount);
                action.putExtra("Action", "Request From");
                startActivity(action);
            }
        });

        SplitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent action = new Intent(PayActions.this, SplitBill.class);
                action.putExtra("Amount", Amount);
                action.putExtra("Action", "Split Bill");
                startActivity(action);
            }
        });

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PayActions.this.finish();
            }
        });

    }
}
