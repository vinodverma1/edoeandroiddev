package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.BanklistAdapter;

import java.util.ArrayList;

public class BankLists extends AppCompatActivity {

    ArrayList<String> BankNameList;
    RecyclerView BankRecyclerview;

    BanklistAdapter adapter;
    ImageView backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banklists);

        backArrow = (ImageView) findViewById(R.id.back_arrow_bankLists);
        BankRecyclerview = (RecyclerView) findViewById(R.id.nameOfBankLists);
        BankRecyclerview.setHasFixedSize(true);
        BankRecyclerview.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        BankNameList = new ArrayList<>();
        BankNameList.clear();

        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");
        BankNameList.add("Bank Of America");

        adapter = new BanklistAdapter(BankLists.this, BankNameList);
        BankRecyclerview.setAdapter(adapter);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BankLists.this.finish();
            }
        });
    }
}
