package com.adarsh.edoe.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.SplitBillAdapter;
import com.adarsh.edoe.app_helper.SessionManager;
import com.adarsh.edoe.database.DatabaseContactList;
import com.adarsh.edoe.handler.ContactHandler;

import java.util.ArrayList;
import java.util.List;

public class SplitBill extends AppCompatActivity {

    public static Button NextBtn;
    ImageView ScanImageviewBtn;

    RecyclerView ContactListView;
    ArrayList<String> ContactNamelist, ContactNumberlist;
    MultiAutoCompleteTextView ContactED;
    ImageView BackArrow;
    String Amount = "";

    SplitBillAdapter adapterContact;
    SessionManager sessionManager;
    DatabaseContactList dbContactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.split_bill);

        sessionManager = new SessionManager(this);
        dbContactList = new DatabaseContactList(this);

        BackArrow = (ImageView) findViewById(R.id.back_arrow_split_bill);
        NextBtn = (Button) findViewById(R.id.nextBtn_split_bill);
        ScanImageviewBtn = (ImageView) findViewById(R.id.scanBtn_split_bill);

        ContactListView = (RecyclerView) findViewById(R.id.contactList_split_bill);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        ContactED = (MultiAutoCompleteTextView) findViewById(R.id.name_search_split_bill);
        ContactED.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();
        ContactNamelist.clear();
        ContactNumberlist.clear();

        try {
            //to hide automatic pop up of soft keyboard
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Amount = getIntent().getStringExtra("Amount");
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SplitBill.this.finish();
            }
        });


        ContactNamelist.add("Thor");
        ContactNamelist.add("Tony Stark");
        ContactNamelist.add("Steve Rogers");
        ContactNamelist.add("Bruce Banner");
        ContactNamelist.add("Natasha Romanoff");
        ContactNamelist.add("Star Lord");
        ContactNamelist.add("Doctor Strange");
        ContactNamelist.add("Wonda Maximoff");
        ContactNamelist.add("Peter");
        ContactNamelist.add("Thanos");

        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");
        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.single_textview, ContactNamelist);
        ContactED.setThreshold(1);//will start working from first character
        ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        ContactED.setTextColor(Color.BLACK);


        adapterContact = new SplitBillAdapter(SplitBill.this, ContactNamelist, ContactNumberlist, "Split", "Split Bill", Amount);
        ContactListView.setAdapter(adapterContact);
    }

    public void GetSearchContent() {

        try {
            List<ContactHandler> Contents = dbContactList.getAllContents();

            for (ContactHandler cn : Contents) {
                // String log = "Id: " + cn.getID() + " ,Name: " + cn.getQuestion_type() + " ,Phone: " + cn.getQestion();

                ContactNamelist.add(cn.getCname());
                ContactNumberlist.add(cn.getCnumber());

            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.single_textview, ContactNamelist);
        ContactED.setThreshold(1);//will start working from first character
        ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        ContactED.setTextColor(Color.BLACK);


        adapterContact = new SplitBillAdapter(SplitBill.this, ContactNamelist, ContactNumberlist, "Split", "Split Bill", Amount);
        ContactListView.setAdapter(adapterContact);

    }
}
