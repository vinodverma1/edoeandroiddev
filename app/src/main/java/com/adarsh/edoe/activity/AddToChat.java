package com.adarsh.edoe.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.AddToChatAdapter;
import com.adarsh.edoe.app_helper.SessionManager;
import com.adarsh.edoe.database.DatabaseContactList;
import com.adarsh.edoe.handler.ContactHandler;

import java.util.ArrayList;
import java.util.List;

public class AddToChat extends AppCompatActivity {

    SessionManager sessionManager;
    DatabaseContactList dbContactList;

    ArrayList<String> ContactNamelist, ContactNumberlist;
    AddToChatAdapter addToChatAdapter;
    RecyclerView ContactListView;
    RelativeLayout relativeAddToChatGroup;
    ImageView BackArrow;
    ArrayList<Drawable> ImageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_to_chat);

        sessionManager = new SessionManager(this);
        dbContactList = new DatabaseContactList(this);

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();
        ImageList = new ArrayList<>();
        ContactNamelist.clear();
        ContactNumberlist.clear();
        ImageList.clear();

        BackArrow = (ImageView) findViewById(R.id.back_arrow_add_to_chat);

        //relativeAddToChatGroup=(RelativeLayout) findViewById(R.id.relative_add_to_chat);
        ContactListView = (RecyclerView) findViewById(R.id.contactList_add_to_chat);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        ContactNamelist.add("New Group Chat");
        ContactNamelist.add("Thor");
        ContactNamelist.add("Tony Stark");
        ContactNamelist.add("Steve Rogers");
        ContactNamelist.add("Bruce Banner");
        ContactNamelist.add("Natasha Romanoff");
        ContactNamelist.add("Star Lord");
        ContactNamelist.add("Doctor Strange");
        ContactNamelist.add("Wonda Maximoff");
        ContactNamelist.add("Peter");
        ContactNamelist.add("Thanos");

        ContactNumberlist.add("0000000000");
        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");
        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");

        ImageList.add(getResources().getDrawable(R.drawable.new_group_2));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));
        ImageList.add(getResources().getDrawable(R.drawable.tony_stark));

        addToChatAdapter = new AddToChatAdapter(AddToChat.this, ContactNamelist, ContactNumberlist, ImageList);
        ContactListView.setAdapter(addToChatAdapter);

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddToChat.this.finish();
            }
        });

    }

    public void GetSearchContent() {

        try {
            List<ContactHandler> Contents = dbContactList.getAllContents();

            for (ContactHandler cn : Contents) {
                // String log = "Id: " + cn.getID() + " ,Name: " + cn.getQuestion_type() + " ,Phone: " + cn.getQestion();

                ContactNamelist.add(cn.getCname());
                ContactNumberlist.add(cn.getCnumber());

            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }

        addToChatAdapter = new AddToChatAdapter(AddToChat.this, ContactNamelist, ContactNumberlist, ImageList);
        ContactListView.setAdapter(addToChatAdapter);
    }
}
