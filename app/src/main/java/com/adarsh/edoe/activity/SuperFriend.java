package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.adarsh.edoe.R;

public class SuperFriend extends AppCompatActivity {
    ImageView back_arrow_super_friend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.super_friends);
        back_arrow_super_friend = (ImageView) findViewById(R.id.back_arrow_super_friend);
        back_arrow_super_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SuperFriend.this.finish();
            }
        });
    }
}
