package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.ChatToContactAdapter;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class ChatToContact extends AppCompatActivity {

    public static ImageView SendBtn;
    public static EditText MessageED;
    SessionManager sessionManager;
    CheckInternet checkInternet;
    RecyclerView ChatRecyclerView;
    ChatToContactAdapter adapter;
    ArrayList<String> UserMessageList, MessageTimeList;
    ImageView VoiceBtn, BackArrow;
    String ContactName = "", Message = "";
    TextView UserNAme;
    TextWatcher watch = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            try {
                if (s.length() > 0) {
                    SendBtn.setVisibility(View.VISIBLE);
                    VoiceBtn.setVisibility(View.GONE);
                } else {
                    VoiceBtn.setVisibility(View.VISIBLE);
                    SendBtn.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_to_contact);

        checkInternet = new CheckInternet(this);
        sessionManager = new SessionManager(this);

        MessageED = (EditText) findViewById(R.id.edttext_chat_to_contact);
        SendBtn = (ImageView) findViewById(R.id.send_chat_to_contact);
        UserNAme = (TextView) findViewById(R.id.userName_chat_to_contact);
        VoiceBtn = (ImageView) findViewById(R.id.voice_chat_to_contact);
        BackArrow = (ImageView) findViewById(R.id.back_arrow_chat_to_contact);

        ChatRecyclerView = (RecyclerView) findViewById(R.id.recycler_chat_to_contact);
        ChatRecyclerView.setHasFixedSize(true);
        ChatRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        try {
            ContactName = getIntent().getStringExtra("ContactName");
            UserNAme.setText(ContactName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        UserMessageList = new ArrayList<>();
        MessageTimeList = new ArrayList<>();
        UserMessageList.clear();
        MessageTimeList.clear();

        UserMessageList.add("Hello!!");
        UserMessageList.add("How are you?");
        UserMessageList.add("I am fine. Thanky you! How are you?");
        UserMessageList.add("I am fine too.");

        MessageTimeList.add("17:40");
        MessageTimeList.add("17:51");
        MessageTimeList.add("17:52");
        MessageTimeList.add("17:59");

        MessageED.addTextChangedListener(watch);

        adapter = new ChatToContactAdapter(ChatToContact.this, UserMessageList, MessageTimeList);
        ChatRecyclerView.setAdapter(adapter);

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatToContact.this.finish();
            }
        });

//        SendBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try{
//                    Message=MessageED.getText().toString().trim();
//                }catch (Exception e){
//                    Message="";
//                    e.printStackTrace();
//                }
//
//                if (TextUtils.isEmpty(Message)){
//                    VoiceBtn.setVisibility(View.VISIBLE);
//                    SendBtn.setVisibility(View.GONE);
//                }else {
//                    UserMessageList.add(Message);
//                    try{
//                        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
//                        Date currentLocalTime = cal.getTime();
//                        DateFormat date = new SimpleDateFormat("HH:mm");
//                        date.setTimeZone(TimeZone.getTimeZone("GMT+5.5"));
//
//                        String localTime = date.format(currentLocalTime);
//                        MessageTimeList.add(localTime);
//                    }catch (Exception e){
//                        MessageTimeList.add(" ");
//                        e.printStackTrace();
//                    }
//                    adapter=new ChatToContactAdapter(ChatToContact.this,UserMessageList,MessageTimeList);
//                    ChatRecyclerView.setAdapter(adapter);
//
//
//                }
//            }
//        });

    }
}
