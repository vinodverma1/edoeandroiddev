package com.adarsh.edoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;

public class AddCardManual extends Activity {

    SessionManager sessionManager;
    CheckInternet checkInternet;
    ImageView back_arrow_Add_card_manual1;

    Button AddCardBtn;
    EditText CardNumberED, ExpirationMonthED, ExpirationYearED, CvvED, ZiopCodeED;
    String CardNumber = "", ExpirationMonth = "", ExpirationYear = "", Cvv = "", ZiopCode = "";
    TextWatcher watch = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            if (s.length() == 2) {
                ExpirationYearED.requestFocus();
            }
        }
    };
    TextWatcher watch1 = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            if (s.length() == 16) {
                ExpirationMonthED.requestFocus();
            }
        }
    };
    TextWatcher watch2 = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            if (s.length() == 2) {
                CvvED.requestFocus();
            }
        }
    };
    TextWatcher watch3 = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            if (s.length() == 3) {
                ZiopCodeED.requestFocus();
            }
        }
    };
    TextWatcher watch4 = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            if (s.length() > 4) {
                AddCardBtn.setBackgroundColor(getResources().getColor(R.color.btn_color));
            } else {
                AddCardBtn.setBackgroundColor(getResources().getColor(R.color.grey));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_card_manual);

        checkInternet = new CheckInternet(this);
        sessionManager = new SessionManager(this);

        CardNumberED = (EditText) findViewById(R.id.card_number_add_card_manual);
        ExpirationMonthED = (EditText) findViewById(R.id.expirationDateMonth_add_card_manual);
        ExpirationYearED = (EditText) findViewById(R.id.expirationDateYear_add_card_manual);
        CvvED = (EditText) findViewById(R.id.cvv_add_card_manual);
        ZiopCodeED = (EditText) findViewById(R.id.zip_code_add_card_manual);
        AddCardBtn = (Button) findViewById(R.id.addCardBtn_add_card_manual);
        back_arrow_Add_card_manual1 = (ImageView) findViewById(R.id.back_arrow_Add_card_manual);

        ExpirationMonthED.addTextChangedListener(watch);
        CardNumberED.addTextChangedListener(watch1);
        ExpirationYearED.addTextChangedListener(watch2);
        CvvED.addTextChangedListener(watch3);
        ZiopCodeED.addTextChangedListener(watch4);


        AddCardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetCardDetails();
            }
        });

        back_arrow_Add_card_manual1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddCardManual.this.finish();
            }
        });

    }

    void GetCardDetails() {
        try {
            CardNumber = CardNumberED.getText().toString().trim();
            ExpirationMonth = ExpirationMonthED.getText().toString().trim();
            ExpirationYear = ExpirationYearED.getText().toString().trim();
            Cvv = CvvED.getText().toString().trim();
            ZiopCode = ZiopCodeED.getText().toString().trim();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(CardNumber)) {
            Toast.makeText(this, "Please Enter Your Card Number", Toast.LENGTH_SHORT).show();
        } else {
            if (CardNumber.length() != 16) {
                Toast.makeText(this, "Please Enter Your 16 Digits Card Number", Toast.LENGTH_SHORT).show();
            } else {

                if (TextUtils.isEmpty(ExpirationMonth)) {
                    Toast.makeText(this, "Please Enter Expiration Month", Toast.LENGTH_SHORT).show();
                } else {
                    int month = 0;
                    try {
                        month = Integer.parseInt(ExpirationMonth);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (month > 12 || month == 00) {
                        Toast.makeText(this, "Please Enter Correct Expiration Month", Toast.LENGTH_SHORT).show();
                    } else {
                        if (TextUtils.isEmpty(ExpirationYear)) {
                            Toast.makeText(this, "Please Enter Expiration Year", Toast.LENGTH_SHORT).show();
                        } else {
                            if (TextUtils.isEmpty(Cvv)) {
                                Toast.makeText(this, "Please Enter CVV", Toast.LENGTH_SHORT).show();
                            } else {
                                if (TextUtils.isEmpty(ZiopCode)) {
                                    Toast.makeText(this, "Please Enter Zip Code", Toast.LENGTH_SHORT).show();
                                } else {
                                    Intent cardd = new Intent(AddCardManual.this, NewCardAdded.class);
                                    startActivity(cardd);
                                    AddCardManual.this.finish();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
