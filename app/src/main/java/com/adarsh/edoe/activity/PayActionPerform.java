package com.adarsh.edoe.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.ContactListAdapter;
import com.adarsh.edoe.app_helper.SessionManager;
import com.adarsh.edoe.database.DatabaseContactList;
import com.adarsh.edoe.handler.ContactHandler;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.ArrayList;
import java.util.List;

public class PayActionPerform extends AppCompatActivity {

    public static Button NextBtn;
    Button HideQrBtn;
    TextView ToolbarName, AmountTV;
    LinearLayout HideQrCode;
    ImageView QRcodeView, ShowQrCode;
    String Toolbar = "", Amount = "";
    ImageView BackArrow;

    RecyclerView ContactListView;
    ArrayList<String> ContactNamelist, ContactNumberlist;
    MultiAutoCompleteTextView ContactED;

    ContactListAdapter adapterContact;
    SessionManager sessionManager;
    DatabaseContactList dbContactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_actions_perform);

        sessionManager = new SessionManager(this);
        dbContactList = new DatabaseContactList(this);

        HideQrBtn = (Button) findViewById(R.id.qrCodeHide_pay_action_perform);
        NextBtn = (Button) findViewById(R.id.nextBtn_pay_action_perform);
        BackArrow = (ImageView) findViewById(R.id.back_arrow_pay_action_perform);
        ShowQrCode = (ImageView) findViewById(R.id.qrCodeShow_pay_action_perform);
        HideQrCode = (LinearLayout) findViewById(R.id.qrCodeShow1_pay_action_perform);
        ToolbarName = (TextView) findViewById(R.id.toolbar_name_pay_action_perform);
        AmountTV = (TextView) findViewById(R.id.amount_pay_action_perform);
        QRcodeView = (ImageView) findViewById(R.id.QRcode_pay_action_perform);


        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PayActionPerform.this.finish();
            }
        });

        ContactListView = (RecyclerView) findViewById(R.id.contactList_pay_action_perform);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        ContactED = (MultiAutoCompleteTextView) findViewById(R.id.name_search_pay_action_perform);
        ContactED.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();
        ContactNamelist.clear();
        ContactNumberlist.clear();

        ContactNamelist.add("Thor");
        ContactNamelist.add("Tony Stark");
        ContactNamelist.add("Steve Rogers");
        ContactNamelist.add("Bruce Banner");
        ContactNamelist.add("Natasha Romanoff");
        ContactNamelist.add("Star Lord");
        ContactNamelist.add("Doctor Strange");
        ContactNamelist.add("Wonda Maximoff");
        ContactNamelist.add("Peter");
        ContactNamelist.add("Thanos");

        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");
        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");

        try {
            Amount = getIntent().getStringExtra("Amount");
            AmountTV.setText(Amount);
            Toolbar = getIntent().getStringExtra("Action");
            ToolbarName.setText(getIntent().getStringExtra("Action"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            //to hide automatic pop up of soft keyboard
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }


        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.single_textview, ContactNumberlist);
        ContactED.setThreshold(1);//will start working from first character
        ContactED.setAdapter(adapter1);//setting the adapter data into the AutoCompleteTextView
        ContactED.setTextColor(Color.BLACK);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.single_textview, ContactNamelist);
        ContactED.setThreshold(1);//will start working from first character
        ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        ContactED.setTextColor(Color.BLACK);


        adapterContact = new ContactListAdapter(PayActionPerform.this, ContactNamelist, ContactNumberlist, "From Action", Toolbar, Amount);
        ContactListView.setAdapter(adapterContact);

        GetQRcode();

//        ContactListView.addOnItemTouchListener(
//                new RecyclerItemClickListener(PayActionPerform.this, ContactListView ,new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//                        // do whatever
//                        Toast.makeText(PayActionPerform.this, ContactNamelist.get(position), Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override public void onLongItemClick(View view, int position) {
//                        // do whatever
//                    }
//                })
//        );

        HideQrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideQrCode.setVisibility(View.GONE);
                ShowQrCode.setVisibility(View.VISIBLE);
            }
        });

        ShowQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideQrCode.setVisibility(View.VISIBLE);
                ShowQrCode.setVisibility(View.GONE);
            }
        });
    }

    public void GetSearchContent() {

        try {
            List<ContactHandler> Contents = dbContactList.getAllContents();

            for (ContactHandler cn : Contents) {
                // String log = "Id: " + cn.getID() + " ,Name: " + cn.getQuestion_type() + " ,Phone: " + cn.getQestion();

                ContactNamelist.add(cn.getCname());
                ContactNumberlist.add(cn.getCnumber());

            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.single_textview, ContactNamelist);
        ContactED.setThreshold(1);//will start working from first character
        ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        ContactED.setTextColor(Color.BLACK);


        adapterContact = new ContactListAdapter(PayActionPerform.this, ContactNamelist, ContactNumberlist, "From Action", Toolbar, Amount);
        ContactListView.setAdapter(adapterContact);


    }

    public void GetQRcode() {
        QRCodeWriter writer = new QRCodeWriter();

        String number = "";
        try {
            number = sessionManager.GetMobileNo();
        } catch (Exception e) {
            number = "";
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(number)) {
            number = "9811871855";
            Toast.makeText(this, "Sorry we cant find your number for qr code", Toast.LENGTH_SHORT).show();
        }
        try {
            BitMatrix bitMatrix = null;
            try {
                bitMatrix = writer.encode(number, BarcodeFormat.QR_CODE, 512, 512);
            } catch (Exception e) {
                e.printStackTrace();
            }
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            QRcodeView.setImageBitmap(bmp);
//            TextView decode_content = (TextView) findViewById(R.id.decode_content);
//            decode_content.setText("Captain Cool");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        GestureDetector mGestureDetector;
        private RecyclerItemClickListener.OnItemClickListener mListener;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, RecyclerItemClickListener.OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && mListener != null) {
                        mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
                return true;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }

        public interface OnItemClickListener {
            public void onItemClick(View view, int position);

            public void onLongItemClick(View view, int position);
        }
    }
}
