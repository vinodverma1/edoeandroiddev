package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;

public class CardInfo extends AppCompatActivity {

    SessionManager sessionManager;
    CheckInternet checkInternet;

    Button ScanUploadBtn;
    TextView CardNumberTV, CardTypeTV, CardExpirationTV;
    String CardNumber = "", CardType = "", CardExpiration = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_info);

        checkInternet = new CheckInternet(this);
        sessionManager = new SessionManager(this);

        ScanUploadBtn = (Button) findViewById(R.id.addCardBtn_card_info);

        CardNumberTV = (TextView) findViewById(R.id.card_number_Card_info);
        CardTypeTV = (TextView) findViewById(R.id.card_type_Card_info);
        CardExpirationTV = (TextView) findViewById(R.id.card_expiration_Card_info);

        ScanUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cardd = new Intent(CardInfo.this, NewCardAdded.class);
                startActivity(cardd);
            }
        });

    }
}
