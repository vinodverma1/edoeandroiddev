package com.adarsh.edoe.activity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class ShowQRcode extends AppCompatActivity {

    ImageView QRCodeView, BackArrow;
    TextView NameTv, NumberTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_qr_code);

        BackArrow = (ImageView) findViewById(R.id.back_arrow_show_qr_code);
        QRCodeView = (ImageView) findViewById(R.id.qrCode_show_qr_code);
        NameTv = (TextView) findViewById(R.id.name_show_qr_code);
        NumberTv = (TextView) findViewById(R.id.number_show_qr_code);

        GetQRcode();

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowQRcode.this.finish();
            }
        });

    }

    public void GetQRcode() {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = null;
            try {
                bitMatrix = writer.encode(NumberTv.getText().toString().trim(), BarcodeFormat.QR_CODE, 512, 512);
            } catch (Exception e) {
                e.printStackTrace();
            }
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            QRCodeView.setImageBitmap(bmp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
