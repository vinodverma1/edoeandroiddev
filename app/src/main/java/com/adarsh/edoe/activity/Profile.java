package com.adarsh.edoe.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckAppPermission;
import com.adarsh.edoe.app_helper.CircleTransform;
import com.adarsh.edoe.app_helper.SessionManager;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;

public class Profile extends AppCompatActivity {

    SessionManager sessionManager;
    TextView CancelBtn, SaveBtn;
    ImageView UserImageView;
    String selectedImagePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        sessionManager = new SessionManager(this);

        UserImageView = (ImageView) findViewById(R.id.userImage_profile);
        SaveBtn = (TextView) findViewById(R.id.saveBtn_profile);
        CancelBtn = (TextView) findViewById(R.id.cancelBtn_profile);

        CancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EnableRuntimePermission();
                Profile.this.finish();
            }
        });

        UserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EnableRuntimePermission();
                choose();
            }
        });

        SaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Profile.this, "Data Saved", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void choose() {

        try {
            final CheckAppPermission checkAppPermission = new CheckAppPermission(Profile.this);

            PackageManager pm = getPackageManager();
            final CharSequence[] items = {"Upload From Gallery", "Upload From Camera", "Remove Profile Picture", "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);
            builder.setTitle("Select Mode");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Upload From Camera")) {

                        try {

                            if (Build.VERSION.SDK_INT >= 23 && !checkAppPermission.checkPermissionForCamera()) {
                                checkAppPermission.requestPermissionForCamera();
                            } else {
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, 100);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (items[item].equals("Upload From Gallery")) {

                        if (Build.VERSION.SDK_INT >= 23 && !checkAppPermission.checkPermissionForReadExternalStorage()) {
                            checkAppPermission.requestPermissionForReadExternalStorage();
                        } else {
                            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 101);
                        }


                    } else if (items[item].equals("Remove Profile Picture")) {

                        UserImageView.setImageDrawable(null);
                        UserImageView.setImageDrawable(getResources().getDrawable(R.drawable.add_picture));

                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                try {
                    UserImageView.setImageDrawable(null);
                    UserImageView.setImageDrawable(getResources().getDrawable(R.drawable.add_picture));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                //UserImageView.setImageBitmap(bitmap);

                /*
                // for getting image uri from bitmap then path from uri
                Uri ImageUri=null;
                try{
                    ImageUri=getImageUri(NavigationMain.this,bitmap);
                }catch (Exception e){
                    e.printStackTrace();
                }

                try{
                    selectedImagePath = getRealPathFromURI(ImageUri);
                }catch (Exception e){
                    e.printStackTrace();
                }
                */

                try {
                    //for image load from url(String)
//                    Glide.with(NavigationMain.this)
//                            .load(selectedImagePath) // add your image url
//                            .transform(new CircleTransform(NavigationMain.this)) // applying the image transformer
//                            .into(UserImageView);


                    //for image load from bitmap
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Glide.with(this)
                            .load(stream.toByteArray())
                            .asBitmap()
                            .transform(new CircleTransform(this))
                            .into(UserImageView);

                } catch (Exception e) {
                }

            } else if (requestCode == 101) {
                try {
                    UserImageView.setImageDrawable(null);
                    UserImageView.setImageDrawable(getResources().getDrawable(R.drawable.add_picture));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);

                try {
//                    Picasso.with(NavigationMain.this)
//                            .load("http://www.masala.com/sites/default/files/styles/gallery_slideshow_cache_734/public/images/2018/04/19/yami-gautam_148091794200.jpg?itok=RIMozQRq" )
//                            .transform(new RoundedImageView())
//                            .resize(100, 100)
//                            .centerCrop()
//                            .into(  UserImageView);

                    Glide.with(Profile.this)
                            .load(picturePath) // add your image url
                            .transform(new CircleTransform(Profile.this)) // applying the image transformer
                            .into(UserImageView);

                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * helper to retrieve the URI of an image bitmap
     */
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        Uri dd = null;
        try {
            dd = Uri.parse(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dd;
    }

    //helper to retrieve the path of an image URI
    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);

            return path;
        }
        // this is our fallback here
        return uri.getPath();
    }
}

