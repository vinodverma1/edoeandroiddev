package com.adarsh.edoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;


public class AddCard extends Activity {

    SessionManager sessionManager;
    CheckInternet checkInternet;

    Button ScanUploadBtn, ManualUploadBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_card);

        checkInternet = new CheckInternet(this);
        sessionManager = new SessionManager(this);

        ScanUploadBtn = (Button) findViewById(R.id.scanCard_add_card);
        ManualUploadBtn = (Button) findViewById(R.id.manualCard_add_card);

        ScanUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ManualUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent add = new Intent(AddCard.this, AddCardManual.class);
                startActivity(add);
                AddCard.this.finish();
            }
        });


    }
}
