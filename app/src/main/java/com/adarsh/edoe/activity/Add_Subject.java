package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.AddSubjectAdapter;

import java.util.ArrayList;

public class Add_Subject extends AppCompatActivity {
    RecyclerView recycler_add_subject;
    ArrayList<String> NameList, ImageUrl;
    AddSubjectAdapter adapter;
    ImageView back_arrow_add_subject;
    Button btn_create_grp_add_subject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_subject);
        back_arrow_add_subject = (ImageView) findViewById(R.id.back_arrow_add_subject);
        btn_create_grp_add_subject = (Button) findViewById(R.id.btn_create_grp_add_subject);
        recycler_add_subject = (RecyclerView) findViewById(R.id.recycler_add_subject);
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(Add_Subject.this);

        mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        recycler_add_subject.setLayoutManager(mLayoutManager1);
        recycler_add_subject.setHasFixedSize(true);
        NameList = new ArrayList<>();
        ImageUrl = new ArrayList<>();

        NameList.add("Alfred");
        NameList.add("Charles");
        NameList.add("Edward");
        adapter = new AddSubjectAdapter(Add_Subject.this, ImageUrl, NameList);
        recycler_add_subject.setAdapter(adapter);
        back_arrow_add_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Add_Subject.this.finish();
            }
        });
        btn_create_grp_add_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Add_Subject.this, SuperFriend.class);
                startActivity(intent);
            }
        });
    }
}
