package com.adarsh.edoe.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.BottomNavigationViewBehaviour;
import com.adarsh.edoe.app_helper.CheckAppPermission;
import com.adarsh.edoe.app_helper.CircleTransform;
import com.adarsh.edoe.fragment.AmountToPay;
import com.adarsh.edoe.fragment.BanksAndCard;
import com.adarsh.edoe.fragment.CameraFragment;
import com.adarsh.edoe.fragment.Chat;
import com.adarsh.edoe.fragment.ContactSupport;
import com.adarsh.edoe.fragment.Feeds;
import com.adarsh.edoe.fragment.GetContacts;
import com.adarsh.edoe.fragment.MainActivity;
import com.adarsh.edoe.fragment.PendingFragment;
import com.adarsh.edoe.fragment.ScanCodeNavFragment;
import com.adarsh.edoe.fragment.Setting;
import com.adarsh.edoe.fragment.TransferFragment;
import com.bumptech.glide.Glide;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;

public class NavigationMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final int RequestPermissionCode = 1;
    private static int SPLASH_TIME_OUT = 5000;
    NavigationView nav_view;
    ImageView UserImageView, QRcodeImageview;
    TextView UsernameTV, MobileTV, AmountTV, TransferAmountTV;
    BottomNavigationView bottomNavigationView;
    View holderView, contentView;
    String selectedImagePath = "", ShowPendingFragment = "";
    private boolean doubleBackToExitPressedOnce = false;


    @SuppressLint("RestrictedApi")
    public static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            //Timber.e(e, "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            //Timber.e(e, "Unable to change value of shift mode");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // .... other stuff in my onResume ....

        this.doubleBackToExitPressedOnce = false;

    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        try {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Click Again to Exit", Toast.LENGTH_SHORT).show();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                doubleBackToExitPressedOnce = false;
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.drawer_icon);

        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
        CoordinatorLayout.LayoutParams layoutParams1 = (CoordinatorLayout.LayoutParams) bottomNavigationView.getLayoutParams();
        layoutParams1.setBehavior(new BottomNavigationViewBehaviour());

        holderView = findViewById(R.id.holder_navigation);
        contentView = findViewById(R.id.content_navigation);

        try {
            Intent showPending = getIntent();
            ShowPendingFragment = showPending.getStringExtra("ShowPendingFragment");
        } catch (Exception e) {
            ShowPendingFragment = "0";
            e.printStackTrace();
        }


        try {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
                final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
                final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                if (i == 2) {
                    // set your height here
                    layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, displayMetrics);
                    // set your width here
                    layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, displayMetrics);
                } else {
                    // set your height here
                    layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, displayMetrics);
                    // set your width here
                    layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, displayMetrics);
                }
                iconView.setLayoutParams(layoutParams);
            }
            disableShiftMode(bottomNavigationView);
            TextView textView = (TextView) bottomNavigationView.findViewById(R.id.bottom_chat).findViewById(R.id.largeLabel);
            textView.setPadding(0, 20, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        nav_view = (NavigationView) findViewById(R.id.nav_view);
        nav_view.setItemIconTintList(null);
        bottomNavigationView.setItemIconTintList(null);

        NavigationMenuView navMenuView = (NavigationMenuView) nav_view.getChildAt(0);

        navMenuView.addItemDecoration(new DividerItemDecoration(NavigationMain.this, DividerItemDecoration.VERTICAL));

        View view = nav_view.getHeaderView(0);

        UserImageView = (ImageView) view.findViewById(R.id.userImage_navigation);
        QRcodeImageview = (ImageView) view.findViewById(R.id.userQRcode_navigation);

        UsernameTV = (TextView) view.findViewById(R.id.name_navigation);
        MobileTV = (TextView) view.findViewById(R.id.number_navigation);
        AmountTV = (TextView) view.findViewById(R.id.amount_navigation);
        TransferAmountTV = (TextView) view.findViewById(R.id.transferToBank_navigation);

        UsernameTV.setText("Adarsh");
        MobileTV.setText("9811871855");
        AmountTV.setText("100,000,000 €");

        TransferAmountTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment selectedFragment = null;
                selectedFragment = new TransferFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        GetQRcode();

        QRcodeImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EnableRuntimePermission();
                Intent qr = new Intent(NavigationMain.this, ShowQRcode.class);
                startActivity(qr);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        UserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EnableRuntimePermission();
                Intent pro = new Intent(NavigationMain.this, Profile.class);
                startActivity(pro);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.bottom_home:
                                selectedFragment = new Feeds();
                                break;
                            case R.id.bottom_chat:
                                selectedFragment = new Chat();
                                break;
                            case R.id.bottom_pay:
                                selectedFragment = new AmountToPay();
                                break;
                            case R.id.action_item2:
                                selectedFragment = new CameraFragment();
                                break;
                            case R.id.action_item3:
                                selectedFragment = new TransferFragment();
                                break;
                        }
                        if (selectedFragment != null) {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_layout, selectedFragment);
                            transaction.commit();
                        }
                        return true;
                    }
                });

        if (ShowPendingFragment == null) {
            //Manually displaying the first fragment - one time only
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, Chat.newInstance());
            transaction.commit();
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, PendingFragment.newInstance());
            transaction.commit();
        }


        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);


        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     if (drawer.isDrawerOpen(navigationView)) {
                                                         drawer.closeDrawer(navigationView);
                                                     } else {
                                                         drawer.openDrawer(navigationView);
                                                     }
                                                 }
                                             }
        );

        drawer.setScrimColor(Color.TRANSPARENT);
        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                                     @Override
                                     public void onDrawerSlide(View drawer, float slideOffset) {


                                         contentView.setX(navigationView.getWidth() * slideOffset);
                                         RelativeLayout.LayoutParams lp =
                                                 (RelativeLayout.LayoutParams) contentView.getLayoutParams();
                                         //lp.height = drawer.getHeight() -
                                         //(int) (drawer.getHeight()  slideOffset  0.3f);
                                         //lp.topMargin = (drawer.getHeight() - lp.height) / 2;
                                         contentView.setLayoutParams(lp);
                                     }

                                     @Override
                                     public void onDrawerClosed(View drawerView) {
                                     }
                                 }
        );
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        int id = item.getItemId();

//        if (id == R.id.nav_home) {
//            fragment = new MainActivity();
//
//        } else
        if (id == R.id.nav_contacts) {
            bottomNavigationView.setVisibility(View.GONE);
            fragment = new GetContacts();

        } else if (id == R.id.nav_pay) {
            bottomNavigationView.setVisibility(View.VISIBLE);
            fragment = new AmountToPay();

        } else if (id == R.id.nav_scancode) {
            bottomNavigationView.setVisibility(View.GONE);
            fragment = new ScanCodeNavFragment();

        } else if (id == R.id.nav_chat) {
            bottomNavigationView.setVisibility(View.VISIBLE);
            fragment = new Chat();
        } else if (id == R.id.nav_feed) {
            bottomNavigationView.setVisibility(View.VISIBLE);
            fragment = new Feeds();

        } else if (id == R.id.nav_banks) {
            bottomNavigationView.setVisibility(View.GONE);
            fragment = new BanksAndCard();
            Bundle args = new Bundle();
            args.putString("AmountToPay", "0");
            fragment.setArguments(args);

        } else if (id == R.id.nav_pendings) {
            bottomNavigationView.setVisibility(View.GONE);
            fragment = new PendingFragment();
        } else if (id == R.id.nav_setting) {
            bottomNavigationView.setVisibility(View.GONE);
            fragment = new Setting();
        } else if (id == R.id.nav_support) {
            bottomNavigationView.setVisibility(View.GONE);
            fragment = new ContactSupport();
        } else if (id == R.id.nav_sign_in) {
            bottomNavigationView.setVisibility(View.VISIBLE);
            fragment = new MainActivity();
        } else if (id == R.id.nav_logout) {

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            NavigationMain.this.finish();

        }

        if (fragment != null) {

            //fragmentTransaction.replace(R.id.content_frame,fragment).commit();
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_layout, fragment).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void GetQRcode() {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = null;
            try {
                bitMatrix = writer.encode(MobileTV.getText().toString(), BarcodeFormat.QR_CODE, 512, 512);
            } catch (Exception e) {
                e.printStackTrace();
            }
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            QRcodeImageview.setImageBitmap(bmp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void choose() {

        try {
            final CheckAppPermission checkAppPermission = new CheckAppPermission(NavigationMain.this);

            PackageManager pm = getPackageManager();
            final CharSequence[] items = {"Upload From Gallery", "Upload From Camera", "Remove Profile Picture", "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(NavigationMain.this);
            builder.setTitle("Select Mode");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Upload From Camera")) {

                        try {

                            if (Build.VERSION.SDK_INT >= 23 && !checkAppPermission.checkPermissionForCamera()) {
                                checkAppPermission.requestPermissionForCamera();
                            } else {
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, 100);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (items[item].equals("Upload From Gallery")) {

                        if (Build.VERSION.SDK_INT >= 23 && !checkAppPermission.checkPermissionForReadExternalStorage()) {
                            checkAppPermission.requestPermissionForReadExternalStorage();
                        } else {
                            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 101);
                        }


                    } else if (items[item].equals("Remove Profile Picture")) {

                        if (TextUtils.isEmpty(selectedImagePath)) {

                            UserImageView.setImageDrawable(getResources().getDrawable(R.drawable.add_picture));
                        } else {
                            UserImageView.setImageDrawable(null);
                            UserImageView.setImageDrawable(getResources().getDrawable(R.drawable.add_picture));
                        }

                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                try {
                    UserImageView.setImageDrawable(null);
                    UserImageView.setImageDrawable(getResources().getDrawable(R.drawable.add_picture));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                //UserImageView.setImageBitmap(bitmap);

                /*
                // for getting image uri from bitmap then path from uri
                Uri ImageUri=null;
                try{
                    ImageUri=getImageUri(NavigationMain.this,bitmap);
                }catch (Exception e){
                    e.printStackTrace();
                }

                try{
                    selectedImagePath = getRealPathFromURI(ImageUri);
                }catch (Exception e){
                    e.printStackTrace();
                }
                */

                try {
                    //for image load from url(String)
//                    Glide.with(NavigationMain.this)
//                            .load(selectedImagePath) // add your image url
//                            .transform(new CircleTransform(NavigationMain.this)) // applying the image transformer
//                            .into(UserImageView);


                    //for image load from bitmap
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Glide.with(this)
                            .load(stream.toByteArray())
                            .asBitmap()
                            .transform(new CircleTransform(this))
                            .into(UserImageView);

                } catch (Exception e) {
                }

            } else if (requestCode == 101) {
                try {
                    UserImageView.setImageDrawable(null);
                    UserImageView.setImageDrawable(getResources().getDrawable(R.drawable.add_picture));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);

                try {
//                    Picasso.with(NavigationMain.this)
//                            .load("http://www.masala.com/sites/default/files/styles/gallery_slideshow_cache_734/public/images/2018/04/19/yami-gautam_148091794200.jpg?itok=RIMozQRq" )
//                            .transform(new RoundedImageView())
//                            .resize(100, 100)
//                            .centerCrop()
//                            .into(  UserImageView);

                    Glide.with(NavigationMain.this)
                            .load(picturePath) // add your image url
                            .transform(new CircleTransform(NavigationMain.this)) // applying the image transformer
                            .into(UserImageView);

                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * helper to retrieve the URI of an image bitmap
     */
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        Uri dd = null;
        try {
            dd = Uri.parse(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dd;
    }

    //helper to retrieve the path of an image URI
    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);

            return path;
        }
        // this is our fallback here
        return uri.getPath();
    }

}

