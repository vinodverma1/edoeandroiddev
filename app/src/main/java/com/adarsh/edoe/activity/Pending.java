package com.adarsh.edoe.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.PendingAdapter;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class Pending extends AppCompatActivity {

    RecyclerView ContactListView;
    ArrayList<String> ContactNamelist, ContactNumberlist;

    PendingAdapter adapterPending;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending);

        sessionManager = new SessionManager(this);

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();

        ContactNamelist.clear();
        ContactNumberlist.clear();

        try {
            ContactNamelist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNameList");
            ContactNumberlist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNumberList");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ContactListView = (RecyclerView) findViewById(R.id.pendingRecycleView_pending);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));

        adapterPending = new PendingAdapter(Pending.this, ContactNamelist, ContactNumberlist);
        ContactListView.setAdapter(adapterPending);

    }
}

