package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.CreateBillAdapter;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class CreateBill extends AppCompatActivity {

    public Button NextBtn;
    RecyclerView ContactListView;
    ArrayList<String> ContactNamelist, ContactNumberlist, SplitInList, PaidByList, PaidNumberlist;
    EditText SubjectED;
    TextView AmountED, PaidBySP, SplitInSP;
    ImageView BackArrow;
    String Subject = "", Amount = "", PaidBy = "", SplitIn = "";

    CreateBillAdapter adapterContact;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_bill);

        sessionManager = new SessionManager(this);

        BackArrow = (ImageView) findViewById(R.id.back_arrow_create_bill);
        NextBtn = (Button) findViewById(R.id.nextBtn_create_bill);
        SplitInSP = (TextView) findViewById(R.id.paidSplit_create_bill);
        PaidBySP = (TextView) findViewById(R.id.paidBy_create_bill);
        SubjectED = (EditText) findViewById(R.id.subject_create_bill);
        AmountED = (TextView) findViewById(R.id.amount_create_bill);

        ContactListView = (RecyclerView) findViewById(R.id.contactList_create_bill);

        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(this);

        mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        ContactListView.setLayoutManager(mLayoutManager1);
        ContactListView.setHasFixedSize(true);

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();
        PaidByList = new ArrayList<>();
        PaidNumberlist = new ArrayList<>();

        ContactNamelist.clear();
        ContactNumberlist.clear();
        PaidByList.clear();
        PaidNumberlist.clear();

        try {
            Amount = getIntent().getStringExtra("Amount");
            AmountED.setText(Amount);
        } catch (Exception e) {
            e.printStackTrace();
        }


        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateBill.this.finish();
            }
        });

        try {
            ContactNamelist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNameList");
            ContactNumberlist = (ArrayList<String>) getIntent().getSerializableExtra("selectedNumberList");
        } catch (Exception e) {
            e.printStackTrace();
        }

        SplitInList = new ArrayList<>();
        SplitInList.add("Equally");
        SplitInList.add("One-third");
        SplitInList.add("One-Fourth");

//        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, R.layout.single_textview, SplitInList);
//        SplitInSP.setAdapter(adapter1);
//        SplitInSP.setPadding(0, 0, 0, 0);
//
//        SplitInSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                SplitIn = SplitInSP.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        PaidByList.add("You");
        for (int i = 0; i < ContactNamelist.size(); i++) {
            PaidByList.add(ContactNamelist.get(i));
        }

        PaidNumberlist.add("9811871855");
        for (int i = 0; i < ContactNumberlist.size(); i++) {
            PaidNumberlist.add(ContactNumberlist.get(i));
        }
//        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, R.layout.single_textview, PaidByList);
//        PaidBySP.setAdapter(adapter2);
////        PaidBySP.setPadding(0, 0, 0, 0);


        PaidBySP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next = new Intent(CreateBill.this, PaidBy.class);
                next.putExtra("selectedNameList", PaidByList);
                next.putExtra("selectedNumberList", PaidNumberlist);
                startActivity(next);
            }
        });

        NextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next = new Intent(CreateBill.this, ConfirmationSplit.class);
                next.putExtra("Amount", Amount);
                next.putExtra("selectedNameList", PaidByList);
                next.putExtra("selectedNumberList", PaidNumberlist);
                startActivity(next);

            }
        });

        adapterContact = new CreateBillAdapter(CreateBill.this, ContactNamelist, ContactNumberlist);
        ContactListView.setAdapter(adapterContact);


    }
}
