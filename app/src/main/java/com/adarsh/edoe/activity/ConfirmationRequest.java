package com.adarsh.edoe.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;
import com.adarsh.edoe.database.DatabaseContactList;


public class ConfirmationRequest extends AppCompatActivity {

    Button OkayBtn;
    SessionManager sessionManager;
    DatabaseContactList dbContactList;
    String Amount = "";
    TextView AmountTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmation_request);

        sessionManager = new SessionManager(this);
        dbContactList = new DatabaseContactList(this);

        OkayBtn = (Button) findViewById(R.id.okay_confirmation_request);
        AmountTV = (TextView) findViewById(R.id.amount_confirmation_send);

        try {
            Amount = getIntent().getStringExtra("Amount");
            AmountTV.setText(Amount);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(ConfirmationRequest.this, PayActions.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ConfirmationRequest.this.finish();
            }
        });
    }
}
