package com.adarsh.edoe.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CheckInternet;

public class Registration extends Activity {

    EditText EmailED, PinED, PasswordED, ConfirmPasswordED;
    String Email = "", Pin = "", Password = "", ConfirmPassword = "";
    CheckInternet checkInternet;
    Button SignUpBtn;
    ImageView back_arrow;
    TextView TermsConditionTV;
    CheckBox checkBoxRegistration;

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        checkInternet = new CheckInternet(this);

        TermsConditionTV = (TextView) findViewById(R.id.termsCondition);
        EmailED = (EditText) findViewById(R.id.edt_email_activity_register);
        PinED = (EditText) findViewById(R.id.edt_pin_activity_register);
        PasswordED = (EditText) findViewById(R.id.edt_password_activity_register);
        ConfirmPasswordED = (EditText) findViewById(R.id.edt_confirmpassword_activity_register);
        SignUpBtn = (Button) findViewById(R.id.signBtn_registration);
        back_arrow = (ImageView) findViewById(R.id.back_arrow_activity_register);
        checkBoxRegistration = (CheckBox) findViewById(R.id.checkBox_registration);

        try {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        TermsConditionTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(50); //You can manage the blinking time with this parameter
                anim.setStartOffset(20);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.ABSOLUTE);
                TermsConditionTV.startAnimation(anim);
            }
        });

        SignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegistrationPatMethod();
            }
        });


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registration.this.finish();
            }
        });

    }

    public void RegistrationPatMethod() {

        try {

            try {

                Email = EmailED.getText().toString().trim();
                Pin = PinED.getText().toString().trim();
                Password = PasswordED.getText().toString().trim();
                ConfirmPassword = ConfirmPasswordED.getText().toString().trim();


            } catch (Exception e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(Email)) {
                Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
            } else {
                if (!isValidEmail(Email)) {
                    Toast.makeText(this, "Please enter your correct email id", Toast.LENGTH_SHORT).show();
                } else {
                    if (TextUtils.isEmpty(Password)) {
                        Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
                    } else {
                        if (ConfirmPassword.equals(Password)) {
                            if (TextUtils.isEmpty(Pin)) {
                                Toast.makeText(this, "Please enter your pin number", Toast.LENGTH_SHORT).show();
                            } else {

                                if (checkBoxRegistration.isChecked()) {
                                    Toast.makeText(this, "Successfully registered. \nPlease login to continue.", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), Login.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    Registration.this.finish();
                                } else {
                                    Toast.makeText(this, "Please select Terms & Conditions", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(this, "Both password are different. Please enter same password", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
