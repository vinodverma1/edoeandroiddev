package com.adarsh.edoe.fragment;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class ShowQRcodeFragment extends Fragment {

    SessionManager sessionManager;
    ImageView QRCodeView;
    TextView NameTv, NumberTv;


//    @Override
//    public void onResume() {
//        super.onResume();
//        getActivity().setTitle(" Pending");
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.show_qr_code_frag, container, false);

        sessionManager = new SessionManager(getActivity());

        QRCodeView = (ImageView) view.findViewById(R.id.qrCode_show_qr_code_frag);
        NameTv = (TextView) view.findViewById(R.id.name_show_qr_code_frag);
        NumberTv = (TextView) view.findViewById(R.id.number_show_qr_code_frag);

        GetQRcodeFrag();

        return view;
    }

    public void GetQRcodeFrag() {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = null;
            try {
                bitMatrix = writer.encode(NumberTv.getText().toString().trim(), BarcodeFormat.QR_CODE, 512, 512);
            } catch (Exception e) {
                e.printStackTrace();
            }
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            QRCodeView.setImageBitmap(bmp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
