package com.adarsh.edoe.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.PayActions;

public class AmountToPay extends Fragment {

    TextView AmountED;
    String Amount = "";
    Button PayBtn;
    LinearLayout OneBtn, TwoBtn, ThreeBtn, FourBtn, FiveBtn, SixBtn, SevenBtn, EightBtn, NineBtn, ZeroBtn, DecimalBtn, DeleteBtn;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Pay");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.amount_to_pay_action_item, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.amount_to_pay, container, false);
        setHasOptionsMenu(true);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        AmountED = (TextView) view.findViewById(R.id.amount_amout_to_pay);
        PayBtn = (Button) view.findViewById(R.id.payBtn_amount_to_pay);

        try {
            OneBtn = (LinearLayout) view.findViewById(R.id.oneBtn_amount_to_pay);
            TwoBtn = (LinearLayout) view.findViewById(R.id.twoBtn_amount_to_pay);
            ThreeBtn = (LinearLayout) view.findViewById(R.id.threeBtn_amount_to_pay);
            FourBtn = (LinearLayout) view.findViewById(R.id.fourBtn_amount_to_pay);
            FiveBtn = (LinearLayout) view.findViewById(R.id.fiveBtn_amount_to_pay);
            SixBtn = (LinearLayout) view.findViewById(R.id.sixBtn_amount_to_pay);
            SevenBtn = (LinearLayout) view.findViewById(R.id.sevenBtn_amount_to_pay);
            EightBtn = (LinearLayout) view.findViewById(R.id.eightBtn_amount_to_pay);
            NineBtn = (LinearLayout) view.findViewById(R.id.nineBtn_amount_to_pay);
            ZeroBtn = (LinearLayout) view.findViewById(R.id.zeroBtn_amount_to_pay);
            DecimalBtn = (LinearLayout) view.findViewById(R.id.decimal_amount_to_pay);
            DeleteBtn = (LinearLayout) view.findViewById(R.id.deleteBtn_amount_to_pay);


            OneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }
                    Amount = Amount + "1";
                    AmountED.setText(Amount + "€");


                }
            });

            TwoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }

                    Amount = Amount + "2";
                    AmountED.setText(Amount + "€");

                }
            });

            ThreeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }
                    Amount = Amount + "3";
                    AmountED.setText(Amount + "€");

                }
            });

            FourBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }

                    Amount = Amount + "4";
                    AmountED.setText(Amount + "€");

                }
            });

            FiveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }

                    Amount = Amount + "5";
                    AmountED.setText(Amount + "€");
                }
            });

            SixBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }

                    Amount = Amount + "6";
                    AmountED.setText(Amount + "€");

                }
            });

            SevenBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }

                    Amount = Amount + "7";
                    AmountED.setText(Amount + "€");
                }
            });

            EightBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }

                    Amount = Amount + "8";
                    AmountED.setText(Amount + "€");

                }
            });

            NineBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }

                    Amount = Amount + "9";
                    AmountED.setText(Amount + "€");


                }
            });

            ZeroBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (AmountED.getText().toString().contains("€")) {
                            Amount = AmountED.getText().toString();
                            int acd = Amount.length();
                            Amount = Amount.substring(0, acd - 1);
                        }
                    } catch (Exception e) {
                        Amount = "";
                        e.printStackTrace();
                    }
                    Amount = Amount + "0";
                    AmountED.setText(Amount + "€");

                }
            });

            DecimalBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (AmountED.getText().length() == 0) {

                    } else {
                        try {
                            if (AmountED.getText().toString().contains("€")) {
                                Amount = AmountED.getText().toString();
                                int acd = Amount.length();
                                Amount = Amount.substring(0, acd - 1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String amt = AmountED.getText().toString().trim();
                        if (amt.contains(".")) {

                        } else {
                            Amount = Amount + ".";
                            AmountED.setText(Amount + "€");

                        }
                    }
                }
            });

            DeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Amount = AmountED.getText().toString();
                        int acd = Amount.length();
                        Amount = Amount.substring(0, acd - 2);

                        if (Amount.length() == 1) {

                            if (Amount.contains("€")) {
                                AmountED.setText(null);
                            } else {
                                AmountED.setText(Amount + "€");
                            }
                        } else {
                            int d = Amount.length();
                            if (d == 0) {
                                AmountED.setText("");
                                AmountED.setHint("€");
                            } else {
                                AmountED.setText(Amount + "€");
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        PayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Amount = AmountED.getText().toString().trim();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (TextUtils.isEmpty(Amount)) {
                    Toast.makeText(getActivity(), "Please Enter Amount", Toast.LENGTH_SHORT).show();
                } else {
                    Fragment fragment = null;
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragment = new BanksAndCard();
                    Bundle args = new Bundle();
                    args.putString("AmountToPay", "1");
                    fragment.setArguments(args);
                    try {
                        fragmentManager.beginTransaction().replace(R.id.frame_layout, fragment).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button

                    Fragment fragment = null;
                    fragment = new Chat();
                    FragmentManager frgManager = getFragmentManager();
                    frgManager.beginTransaction().remove(AmountToPay.this).replace(R.id.frame_layout, fragment).commit();
                    return true;

                }
                return false;
            }
        });

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_pay_amount:
                if (TextUtils.isEmpty(AmountED.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please Enter Amount", Toast.LENGTH_SHORT).show();
                } else {
                    Intent pay = new Intent(getActivity(), PayActions.class);
                    pay.putExtra("amount", AmountED.getText().toString().trim());
                    startActivity(pay);
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
