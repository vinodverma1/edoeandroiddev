package com.adarsh.edoe.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.MeAdapter;

import java.util.ArrayList;

public class BetweenYou extends Fragment {

    ArrayList<String> PostDate, PostType, MessagesCount, HeartCount, ActivityPost, ActivityMessage, Money, ImageUrl;

    RecyclerView recyclerViewMe;
    MeAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.me, container, false);

        PostDate = new ArrayList<>();
        PostType = new ArrayList<>();
        MessagesCount = new ArrayList<>();
        HeartCount = new ArrayList<>();
        ActivityPost = new ArrayList<>();
        ActivityMessage = new ArrayList<>();
        Money = new ArrayList<>();
        ImageUrl = new ArrayList<>();

        PostDate.add("2/12/2017");
        PostDate.add("5/04/2018");
        PostDate.add("1/2/2018");

        PostType.add("Public");
        PostType.add("Private");
        PostType.add("Public");

        MessagesCount.add("5");
        MessagesCount.add("10");
        MessagesCount.add("7");

        HeartCount.add("11");
        HeartCount.add("9");
        HeartCount.add("3");

        ActivityPost.add("Having Fun");
        ActivityPost.add("Dinner Time");
        ActivityPost.add("Watching Cricket");

        ActivityMessage.add("Watching movie with friends, late night movie, fun with friends");
        ActivityMessage.add("Having dinner with family,  family time");
        ActivityMessage.add("Its India vs Australia world cup final, #bleedblue,  India!!!!!");

        Money.add("$300");
        Money.add("$150");
        Money.add("$500");


        adapter = new MeAdapter(getActivity(), ImageUrl, ActivityPost, ActivityMessage, PostDate, PostType, Money, MessagesCount, HeartCount);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewMe = (RecyclerView) view.findViewById(R.id.recyclerView_me_main);
        recyclerViewMe.setLayoutManager(layoutManager);
        recyclerViewMe.setItemAnimator(new DefaultItemAnimator());
        //recyclerViewMe.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));
        recyclerViewMe.setAdapter(adapter);

        return view;
    }
}
