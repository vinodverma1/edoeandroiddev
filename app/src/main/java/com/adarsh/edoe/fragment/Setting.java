package com.adarsh.edoe.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;

public class Setting extends Fragment {

    SessionManager sessionManager;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Settings");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting, container, false);

        sessionManager = new SessionManager(getActivity());

        return view;
    }
}

