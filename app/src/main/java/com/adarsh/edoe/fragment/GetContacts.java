package com.adarsh.edoe.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.ContactListAdapter;
import com.adarsh.edoe.app_helper.CheckInternet;
import com.adarsh.edoe.app_helper.SessionManager;
import com.adarsh.edoe.database.DatabaseContactList;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class GetContacts extends Fragment {

    SessionManager sessionManager;
    CheckInternet checkInternet;
    ContactListAdapter adapterContact;

    RecyclerView ContactListView;
    ArrayList<String> ContactNamelist, ContactNumberlist;
    MultiAutoCompleteTextView ContactED;
    DatabaseContactList dbContactList;
    private int PERMISSION_REQUEST_CONTACT = 100;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(" Search Contacts");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.get_contacts, container, false);

        checkInternet = new CheckInternet(getActivity());
        sessionManager = new SessionManager(getActivity());
        dbContactList = new DatabaseContactList(getActivity());

        ContactListView = (RecyclerView) view.findViewById(R.id.contactList);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));

        ContactED = (MultiAutoCompleteTextView) view.findViewById(R.id.name_search_contactList);
        ContactED.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();
        ContactNamelist.clear();
        ContactNumberlist.clear();

        try {
            //to hide automatic pop up of soft keyboard
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //askForContactPermission();

        ContactNamelist.add("Thor");
        ContactNamelist.add("Tony Stark");
        ContactNamelist.add("Steve Rogers");
        ContactNamelist.add("Bruce Banner");
        ContactNamelist.add("Natasha Romanoff");
        ContactNamelist.add("Star Lord");
        ContactNamelist.add("Doctor Strange");
        ContactNamelist.add("Wonda Maximoff");
        ContactNamelist.add("Peter");
        ContactNamelist.add("Thanos");

        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");
        ContactNumberlist.add("9865321478");
        ContactNumberlist.add("7845123698");
        ContactNumberlist.add("7946138521");
        ContactNumberlist.add("1593575620");
        ContactNumberlist.add("9874563210");

        for (int g = 0; g < ContactNamelist.size(); g++) {

            String cont = "";
            try {
                cont = dbContactList.getContent1(ContactNamelist.get(g));
            } catch (Exception e) {
                cont = "";
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(cont)) {

            } else {
                boolean check = dbContactList.addContentQues(ContactNamelist.get(g), ContactNumberlist.get(g));
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.single_textview, ContactNamelist);

        ContactED.setThreshold(1);//will start working from first character
        ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        ContactED.setTextColor(Color.BLACK);

        adapterContact = new ContactListAdapter(getActivity(), ContactNamelist, ContactNumberlist, "From Contact", "", "");
        ContactListView.setAdapter(adapterContact);


        ContactED.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = parent.getItemAtPosition(position).toString();
                Toast.makeText(getActivity(), name, Toast.LENGTH_SHORT).show();
            }
        });

//        ContactListView.addOnItemTouchListener(
//                new RecyclerItemClickListener(GetContacts.this, ContactListView ,new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//                        // do whatever
//                        Toast.makeText(GetContacts.this, ContactNamelist.get(position), Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override public void onLongItemClick(View view, int position) {
//                        // do whatever
//                    }
//                })
//        );

        return view;
    }

    private ArrayList<String> getContactList() {

        ArrayList<String> stringArrayList = new ArrayList<>();
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));


                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));


                        ContactNumberlist.add(phoneNo);
                        stringArrayList.add(name);
                        try {
                            boolean check = false;
                            String CName = "";

                            try {
                                CName = dbContactList.getContent1(phoneNo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (TextUtils.isEmpty(CName)) {
                                check = dbContactList.addContentQues(name, phoneNo);
                            }
                            try {
                                if (ContactNumberlist.size() > 15) {
                                    break;
                                }
                            } catch (Exception r) {
                                r.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "Name: " + name);
                        Log.i(TAG, "Phone Number: " + phoneNo);
                    }
                    pCur.close();
                }
            }
        }
        if (cur != null) {
            cur.close();
        }

        return stringArrayList;
    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                try {
                    ContactNamelist = getContactList();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.single_textview, ContactNamelist);

                    ContactED.setThreshold(1);//will start working from first character
                    ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                    ContactED.setTextColor(Color.BLACK);

                    adapterContact = new ContactListAdapter(getActivity(), ContactNamelist, ContactNumberlist, "From Contact", "", "");
                    ContactListView.setAdapter(adapterContact);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                ContactNamelist = getContactList();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.single_textview, ContactNamelist);

                ContactED.setThreshold(1);//will start working from first character
                ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                ContactED.setTextColor(Color.BLACK);

                adapterContact = new ContactListAdapter(getActivity(), ContactNamelist, ContactNumberlist, "From Contact", "", "");
                ContactListView.setAdapter(adapterContact);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CONTACT) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    ContactNamelist = getContactList();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.single_textview, ContactNamelist);

                    ContactED.setThreshold(1);//will start working from first character
                    ContactED.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                    ContactED.setTextColor(Color.BLACK);

                    adapterContact = new ContactListAdapter(getActivity(), ContactNamelist, ContactNumberlist, "From Contact", "", "");
                    ContactListView.setAdapter(adapterContact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // permission was granted, yay! Do the
                // contacts-related task you need to do.

            } else {
                Toast.makeText(getActivity(), "No permission for contacts", Toast.LENGTH_SHORT).show();
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        }
    }

    public static class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        GestureDetector mGestureDetector;
        private OnItemClickListener mListener;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && mListener != null) {
                        mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
                return true;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }

        public interface OnItemClickListener {
            public void onItemClick(View view, int position);

            public void onLongItemClick(View view, int position);
        }
    }
}
