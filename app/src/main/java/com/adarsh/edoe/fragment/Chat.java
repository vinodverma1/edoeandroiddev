package com.adarsh.edoe.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.AddToChat;
import com.adarsh.edoe.adapter.ChatAdapter;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class Chat extends Fragment {

    ChatAdapter chatAdapter;
    SessionManager sessionManager;

    RecyclerView ChatListView;
    ArrayList<String> NameList, SingleMessageList, TimeDurationList;

    public static Chat newInstance() {
        Chat fragment = new Chat();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Chat");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.chat_add_contact, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat, container, false);

        sessionManager = new SessionManager(getActivity());
        setHasOptionsMenu(true);

        NameList = new ArrayList<>();
        SingleMessageList = new ArrayList<>();
        TimeDurationList = new ArrayList<>();

        NameList.clear();
        SingleMessageList.clear();
        TimeDurationList.clear();

        NameList.add("Thor");
        NameList.add("Tony Stark");
        NameList.add("Steve Rogers");
        NameList.add("Bruce Banner");
        NameList.add("Natasha Romanoff");
        NameList.add("Thor");
        NameList.add("Tony Stark");
        NameList.add("Steve Rogers");
        NameList.add("Bruce Banner");
        NameList.add("Natasha Romanoff");


        SingleMessageList.add("Watching movie with friends, late night movie, fun with friends");
        SingleMessageList.add("Having dinner with family,  family time");
        SingleMessageList.add("Its India vs Australia world cup final, #bleedblue,  India!!!!!");
        SingleMessageList.add("Watching Avengers with friends");
        SingleMessageList.add("Swimming is best exercide");
        SingleMessageList.add("Watching movie with friends, late night movie, fun with friends");
        SingleMessageList.add("Having dinner with family,  family time");
        SingleMessageList.add("Its India vs Australia world cup final, #bleedblue,  India!!!!!");
        SingleMessageList.add("Watching Avengers with friends");
        SingleMessageList.add("Swimming is best exercide");

        TimeDurationList.add("30 mins");
        TimeDurationList.add("1 hr");
        TimeDurationList.add("Monday");
        TimeDurationList.add("2/16/18");
        TimeDurationList.add("1/20/18");
        TimeDurationList.add("30 mins");
        TimeDurationList.add("1 hr");
        TimeDurationList.add("Monday");
        TimeDurationList.add("2/16/18");
        TimeDurationList.add("1/20/18");

        ChatListView = (RecyclerView) view.findViewById(R.id.pendingRecycleView_chat);
        ChatListView.setHasFixedSize(true);
        ChatListView.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));

        chatAdapter = new ChatAdapter(getActivity(), NameList, SingleMessageList, TimeDurationList);
        ChatListView.setAdapter(chatAdapter);


        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_chat_add_contact:
                Intent pay = new Intent(getActivity(), AddToChat.class);
                startActivity(pay);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
