package com.adarsh.edoe.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.AddPaymentCard;
import com.adarsh.edoe.adapter.BankAndCardAdapter2;
import com.adarsh.edoe.adapter.BanksAndCardAdapter;

import java.util.ArrayList;

public class BanksAndCard extends Fragment {

    Button ProceedBtn;
    RecyclerView DebitCardRecyclerview, BankRecyclerview;

    ArrayList<String> DebitCardList, BankList;
    BanksAndCardAdapter banksAndCardAdapter;
    BankAndCardAdapter2 bankAndCardAdapter2;
    String value = "0";

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Banks & Cards");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.banks_and_card, container, false);

        ProceedBtn = (Button) view.findViewById(R.id.add_payment_bankAndCard);


        DebitCardRecyclerview = (RecyclerView) view.findViewById(R.id.cardListView_bankAndCard);
        DebitCardRecyclerview.setHasFixedSize(true);
        DebitCardRecyclerview.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));

        BankRecyclerview = (RecyclerView) view.findViewById(R.id.bankListView_bankAndCard);
        BankRecyclerview.setHasFixedSize(true);
        BankRecyclerview.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));

        try {
            value = getArguments().getString("AmountToPay");
        } catch (Exception e) {
            value = "0";
            e.printStackTrace();
        }

        DebitCardList = new ArrayList<>();
        BankList = new ArrayList<>();

        DebitCardList.clear();
        BankList.clear();

        DebitCardList.add("...4589");
        DebitCardList.add("...7896");
        DebitCardList.add("...8956");
        DebitCardList.add("...7532");

        BankList.add("KOTAK BANK");
        BankList.add("PUNJAB NATIONAL BANK");
        BankList.add("ICICI BANK");
        BankList.add("INDUSIND BANK");

        ProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent amount = new Intent(getActivity(), AddPaymentCard.class);
                startActivity(amount);
            }
        });

        banksAndCardAdapter = new BanksAndCardAdapter(getActivity(), DebitCardList);
        DebitCardRecyclerview.setAdapter(banksAndCardAdapter);

        bankAndCardAdapter2 = new BankAndCardAdapter2(getActivity(), BankList);
        BankRecyclerview.setAdapter(bankAndCardAdapter2);

        ProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent card = new Intent(getActivity(), AddPaymentCard.class);
                startActivity(card);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button

                    if (value.equalsIgnoreCase("1")) {
                        FragmentManager fm = getFragmentManager();
                        if (fm.getBackStackEntryCount() > 0) {
                            Log.i("MainActivity", "popping backstack");
                            fm.popBackStack();
                        } else {
                            Log.i("MainActivity", "nothing on backstack, calling super");
                        }
                    } else {
                        Fragment fragment = null;
                        Bundle args = new Bundle();
                        fragment = new MainActivity();
                        args.putString("Category Content", null);
                        fragment.setArguments(args);
                        FragmentManager frgManager = getFragmentManager();
                        frgManager.beginTransaction().remove(BanksAndCard.this).replace(R.id.frame_layout, fragment).commit();
                        return true;
                    }

                }
                return false;
            }
        });

        return view;
    }
}
