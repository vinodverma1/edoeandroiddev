package com.adarsh.edoe.fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.CameraPreview;
import com.adarsh.edoe.app_helper.CheckAppPermission;
import com.adarsh.edoe.app_helper.SessionManager;

public class CameraFragment extends Fragment {

    SessionManager sessionManager;
    int maxFlash = 0, minFlash = 0;
    boolean CheckForFlash = false;
    private Camera mCamera;
    private CameraPreview mPreview;

//    @Override
//    public void onResume() {
//        try{
//            super.onResume();
//            getActivity().setTitle("Camera");
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);

        boolean check = checkCameraHardware(getActivity());

        if (check) {
            // Create an instance of Camera
            mCamera = getCameraInstance();

            CheckForFlash = getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
            if (CheckForFlash) {

                try {
                    /*
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        //Request for permission for external storage

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                    100);
                        }else{
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                    100);
                        }

                    } else {
                        Camera.Parameters parametro = mCamera.getParameters();
                        parametro.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        mCamera.setParameters(parametro);
                        mCamera.startPreview();
                    } */

                    final CheckAppPermission checkAppPermission = new CheckAppPermission(getActivity());
                    if (Build.VERSION.SDK_INT >= 23 && !checkAppPermission.checkPermissionForCamera()) {
                        checkAppPermission.requestPermissionForCamera();
                    } else {
                        Camera.Parameters parametro = mCamera.getParameters();
                        parametro.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        mCamera.setParameters(parametro);
                        mCamera.startPreview();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(getActivity(), "Your Phone Does Not Support Camera", Toast.LENGTH_SHORT).show();
        }

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(getActivity(), mCamera);
        FrameLayout preview = (FrameLayout) view.findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        return view;
    }

    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
}
