package com.adarsh.edoe.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.TransferBalanceConfirmation;
import com.adarsh.edoe.app_helper.SessionManager;

public class TransferFragment extends Fragment {

    SessionManager sessionManager;
    EditText AmountED;
    boolean check = false;
    private String Amount = "";
    TextWatcher watch = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            try {
                if (check) {

                } else {
                    Amount = String.valueOf(s);
                    int acd = Amount.length();
                    if (acd == 1) {
                        AmountED.setText(Amount + "€");
                    } else {
                        Amount = Amount.substring(0, acd - 1);
                        AmountED.setText(Amount + "€");
                    }

                }

                Amount = AmountED.getText().toString();
                int acd = Amount.length();
                Amount = Amount.substring(0, acd - 1);
                AmountED.setText(Amount + "€");
                //AmountED.setSelection(AmountED.getText().length());

//                try{
//                    if (acd==0){
//                        AmountED.setText(Amount+"");
//                        AmountED.setSelection(AmountED.getText().length());
//                    }else {
//                        Amount = Amount.substring(0, acd -2);
//                        AmountED.setText(Amount+"€");
//                        AmountED.setSelection(AmountED.getText().length());
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Transfer Balance");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.amount_to_pay_action_item, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transfer_fragment, container, false);
        setHasOptionsMenu(true);

        sessionManager = new SessionManager(getActivity());

        AmountED = (EditText) view.findViewById(R.id.amount_transfer_fragment);

        AmountED.setEnabled(false);
        AmountED.addTextChangedListener(watch);

        AmountED.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // You can identify which key pressed buy checking keyCode value
                // with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    // this is for backspace

                    try {
                        check = true;
                        Amount = AmountED.getText().toString();
                        int acd = Amount.length();
                        if (acd == 0) {
                        } else {
                            Amount = Amount.substring(0, acd - 2);
                            AmountED.setText(Amount + "€");
                            AmountED.setSelection(AmountED.getText().length());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("IME_TEST", "DEL KEY");
                } else {
                    check = false;
                }
                return false;
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button

                    Fragment fragment = null;
                    fragment = new Chat();
                    FragmentManager frgManager = getFragmentManager();
                    frgManager.beginTransaction().remove(TransferFragment.this).replace(R.id.frame_layout, fragment).commit();
                    return true;

                }
                return false;
            }
        });


        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_pay_amount:
                Intent tran = new Intent(getActivity(), TransferBalanceConfirmation.class);
                startActivity(tran);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
