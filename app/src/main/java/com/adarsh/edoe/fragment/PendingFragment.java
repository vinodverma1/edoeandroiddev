package com.adarsh.edoe.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adarsh.edoe.R;
import com.adarsh.edoe.adapter.PendingAdapter;
import com.adarsh.edoe.app_helper.SessionManager;

import java.util.ArrayList;

public class PendingFragment extends Fragment {

    RecyclerView ContactListView;
    ArrayList<String> ContactNamelist, ContactNumberlist;

    PendingAdapter adapterPending;
    SessionManager sessionManager;

    public static PendingFragment newInstance() {
        PendingFragment fragment = new PendingFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(" Pending");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pending_fragment, container, false);

        sessionManager = new SessionManager(getActivity());

        ContactNamelist = new ArrayList<>();
        ContactNumberlist = new ArrayList<>();

        ContactNamelist.clear();
        ContactNumberlist.clear();

        ContactNamelist.add("Roman");
        ContactNamelist.add("Brock");

        ContactNumberlist.add("9855236586");
        ContactNumberlist.add("7896541256");

        ContactListView = (RecyclerView) view.findViewById(R.id.pendingRecycleView_pending_fragment);
        ContactListView.setHasFixedSize(true);
        ContactListView.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));

        adapterPending = new PendingAdapter(getActivity(), ContactNamelist, ContactNumberlist);
        ContactListView.setAdapter(adapterPending);

        return view;
    }
}
