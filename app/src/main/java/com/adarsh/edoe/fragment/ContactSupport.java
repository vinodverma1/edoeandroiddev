package com.adarsh.edoe.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;

public class ContactSupport extends Fragment {

    SessionManager sessionManager;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(" Contact Support");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_support, container, false);

        sessionManager = new SessionManager(getActivity());

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button

                    Fragment fragment = null;
                    fragment = new Chat();
                    FragmentManager frgManager = getFragmentManager();
                    frgManager.beginTransaction().remove(ContactSupport.this).replace(R.id.frame_layout, fragment).commit();
                    return true;

                }
                return false;
            }
        });
        return view;
    }
}

