package com.adarsh.edoe.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;

import com.adarsh.edoe.R;
import com.adarsh.edoe.app_helper.SessionManager;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class QRCodeScanner extends Fragment {
    private static int SPLASH_TIME_OUT = 2000;
    SessionManager sessionManager;
    int CameraFrontId;
    TextToSpeech speech;
    IntentResult scan;
    private IntentIntegrator integrator;
    private String CodeId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentIntegrator integrator = new IntentIntegrator(this.getActivity()).forSupportFragment(this);
        // use forSupportFragment or forFragment method to use fragments instead of activity
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt(this.getString(R.string.scan_bar_code));
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.initiateScan();
    }

    public void startCamera(int camera_id, boolean beep, String code_format) {

        try {
            integrator.setBeepEnabled(beep);
            integrator.setCameraId(camera_id);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.initiateScan();
        } catch (Exception w) {
            w.printStackTrace();
        }
//        if(code_format.equals("QR_CODE_TYPES"))
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
//        if(code_format.equals("ONE_D_CODE_TYPES"))
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
//        if(code_format.equals("PRODUCT_CODE_TYPES"))
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.PRODUCT_CODE_TYPES);
//        if(code_format.equals("DATA_MATRIX_TYPES"))
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.DATA_MATRIX_TYPES);
//        if(code_format.equals("ALL_CODE_TYPES"))
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
//        if(code_format.equals("Face Detection"))
//        {
//            speech.speak("For future use", TextToSpeech.QUEUE_FLUSH,null);
//            return ;
//        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanningResult != null) {
            //we have a result
            String codeContent = scanningResult.getContents();
            String codeFormat = scanningResult.getFormatName();

        } else {

        }
    }
}
