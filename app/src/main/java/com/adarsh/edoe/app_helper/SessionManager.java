package com.adarsh.edoe.app_helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    String LoginRemember = "", MobileNo = "", EmailRemember = "", UserName = "";

    Context context;

    public SessionManager(Context cntxt) {
        this.context = cntxt;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void Savepreferences(String key, String Value) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = pref.edit();
        editor.putString(key, Value);
        editor.commit();
    }

    public String GetLoginRemember() {
        LoginRemember = (pref.getString("LoginRemember", ""));
        return LoginRemember;
    }

    public String GetEmailRemember() {
        EmailRemember = (pref.getString("EmailRemember", ""));
        return EmailRemember;
    }

    public String GetMobileNo() {
        MobileNo = (pref.getString("MobileNo", ""));
        return MobileNo;
    }

    public String GetUsername() {
        UserName = (pref.getString("UserName", ""));
        return UserName;
    }
}

