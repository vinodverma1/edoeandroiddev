package com.adarsh.edoe.handler;


public class ContactHandler {

    //private variables
    int _id;
    String _contact_name = "", _contact_number = "";

    // Empty constructor
    public ContactHandler() {

    }

    // constructor
    public ContactHandler(int id, String source, String destination) {
        this._id = id;
        this._contact_name = source;
        this._contact_number = destination;
    }

    // constructor
    public ContactHandler(String source, String destination) {
        this._contact_name = source;
        this._contact_number = destination;
    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    public String getCname() {
        return this._contact_name;
    }

    public void setCname(String _contact_name) {
        this._contact_name = _contact_name;
    }

    public String getCnumber() {
        return this._contact_number;
    }

    public void setCnumber(String _contact_number) {
        this._contact_number = _contact_number;
    }

}
