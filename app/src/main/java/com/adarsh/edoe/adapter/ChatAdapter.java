package com.adarsh.edoe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<String> ContactNameList, ContentPaid, UserImage, TimeDuration;

    public ChatAdapter(Context context, ArrayList<String> NameList, ArrayList<String> ContentPaid1,
                       ArrayList<String> TimeDuration1) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContentPaid = new ArrayList<String>();
        UserImage = new ArrayList<>();
        TimeDuration = new ArrayList<>();

        this.ContactNameList = NameList;
        this.ContentPaid = ContentPaid1;
        this.TimeDuration = TimeDuration1;
    }

    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_adapter, parent, false);

        return new ChatAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ChatAdapter.MyViewHolder holder, final int position) {

        holder.nameTV.setText(ContactNameList.get(position).toString());
        holder.singleMessage.setText(ContentPaid.get(position).toString());
        holder.timeDurationTV.setText(TimeDuration.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTV, timeDurationTV, singleMessage;
        ImageView tick, UserImage;
        LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);

            linearLayout = (LinearLayout) view.findViewById(R.id._chat_adapter);
            nameTV = (TextView) view.findViewById(R.id.contactName_chat_adapter);
            timeDurationTV = (TextView) view.findViewById(R.id.chatTimeInterval_chat_adapter);
            singleMessage = (TextView) view.findViewById(R.id.message_single_chat_adapter);
            tick = (ImageView) view.findViewById(R.id.tick_chat_adapter);
            UserImage = (ImageView) view.findViewById(R.id.contactImage_chat_adapter);
        }
    }
}


