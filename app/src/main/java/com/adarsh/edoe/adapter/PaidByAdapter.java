package com.adarsh.edoe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class PaidByAdapter extends RecyclerView.Adapter<PaidByAdapter.MyViewHolder> {

    public int lastSelectedPosition = -1;
    Context mContext;
    ArrayList<String> ContactNameList, ContactnumberList, UserImage;

    public PaidByAdapter(Context context, ArrayList<String> NameList, ArrayList<String> ContactnumberList1) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContactnumberList = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.ContactNameList = NameList;
        this.ContactnumberList = ContactnumberList1;


    }

    @Override
    public PaidByAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.paid_by_adapter, parent, false);

        return new PaidByAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PaidByAdapter.MyViewHolder holder, final int position) {

        try {
            //since only one radio button is allowed to be selected,
            // this condition un-checks previous selections
            holder.radioButton.setChecked(lastSelectedPosition == position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.nameTV.setText(ContactNameList.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTV;
        ImageView UserImage;
        RadioButton radioButton;

        public MyViewHolder(View view) {
            super(view);

            nameTV = (TextView) view.findViewById(R.id.userName_paid_by_adapter);
            UserImage = (ImageView) view.findViewById(R.id.userImage_paid_by_adapter);
            radioButton = (RadioButton) view.findViewById(R.id.radioBtn_paid_by_adapter);

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                    try {
                        Toast.makeText(mContext,
                                ContactNameList.get(lastSelectedPosition).toString(),
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}


