package com.adarsh.edoe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.ChatToContact;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ChatToContactAdapter extends RecyclerView.Adapter<ChatToContactAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<String> MessageList, TimeList, UserImage;
    int DemoCheck = 0;
    String Message = "";

    public ChatToContactAdapter(Context context, ArrayList<String> NameList, ArrayList<String> TimeList1) {
        mContext = context;

        MessageList = new ArrayList<String>();
        TimeList = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.MessageList = NameList;
        this.TimeList = TimeList1;
    }

    @Override
    public ChatToContactAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.super_friend_single_item, parent, false);

        return new ChatToContactAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ChatToContactAdapter.MyViewHolder holder, final int position) {

        // holder.UsernameTV.setText(MessageList.get(position).toString());
        if (DemoCheck == 0) {
            holder.ContactRelative.setVisibility(View.GONE);
            holder.UserImage.setVisibility(View.GONE);
            holder.UserRelative.setVisibility(View.VISIBLE);
            holder.UserMessageTV.setText(MessageList.get(position).toString());
            holder.UserDateTV.setText(TimeList.get(position).toString());
            DemoCheck = 1;
        } else if (DemoCheck == 1) {
            holder.ContactRelative.setVisibility(View.VISIBLE);
            holder.UserImage.setVisibility(View.VISIBLE);
            holder.UserRelative.setVisibility(View.GONE);
            holder.ContactMessageTV.setText(MessageList.get(position).toString());
            holder.ContactDateT.setText(TimeList.get(position).toString());
            DemoCheck = 0;
        }

        ChatToContact.SendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Message = ChatToContact.MessageED.getText().toString().trim();
                } catch (Exception e) {
                    Message = "";
                    e.printStackTrace();
                }

                if (TextUtils.isEmpty(Message)) {
//                    VoiceBtn.setVisibility(View.VISIBLE);
//                    ChatToContact.SendBtn.setVisibility(View.GONE);
                } else {
                    MessageList.add(Message);
                    try {
                        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5.5"));
                        Date currentLocalTime = cal.getTime();
                        DateFormat date = new SimpleDateFormat("HH:mm");
                        date.setTimeZone(TimeZone.getTimeZone("GMT+5.5"));

                        String localTime = date.format(currentLocalTime);
                        TimeList.add(localTime);
                    } catch (Exception e) {
                        TimeList.add(" ");
                        e.printStackTrace();
                    }
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    adapter=new ChatToContactAdapter(ChatToContact.this,UserMessageList,MessageTimeList);
//                    ChatRecyclerView.setAdapter(adapter);


                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return MessageList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView UserDateTV, ContactDateT, UserMessageTV, ContactMessageTV;
        ImageView UserImage;
        RelativeLayout UserRelative, ContactRelative;

        public MyViewHolder(View view) {
            super(view);

            UserImage = (ImageView) view.findViewById(R.id.imgvw_super_frnd_single_item);
            UserDateTV = (TextView) view.findViewById(R.id.userTime_super_frnd_single_item);
            UserMessageTV = (TextView) view.findViewById(R.id.userMessage_super_frnd_single_item);
            ContactDateT = (TextView) view.findViewById(R.id.contactTime_super_frnd_single_item);
            ContactMessageTV = (TextView) view.findViewById(R.id.contactMessage_super_frnd_single_item);
            UserRelative = (RelativeLayout) view.findViewById(R.id.relative_user_message);
            ContactRelative = (RelativeLayout) view.findViewById(R.id.relative_contact_message);

        }
    }
}


