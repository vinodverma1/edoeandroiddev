package com.adarsh.edoe.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class MeDetailAdapter extends RecyclerView.Adapter<MeDetailAdapter.MyViewHolder> {

    private ArrayList<String> mImagesList, PostDate, PostLike, ActivityPost, ActivityMessage;
    private Context mContext;

    public MeDetailAdapter(Context context, ArrayList<String> imageList, ArrayList<String> activity, ArrayList<String> activityMessage,
                           ArrayList<String> postDate, ArrayList<String> postLike) {

        mImagesList = new ArrayList<String>();
        PostDate = new ArrayList<String>();
        PostLike = new ArrayList<String>();
        ActivityPost = new ArrayList<String>();
        ActivityMessage = new ArrayList<String>();


        mContext = context;
        this.mImagesList = imageList;
        this.ActivityPost = activity;
        this.ActivityMessage = activityMessage;
        this.PostDate = postDate;
        this.PostLike = postLike;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_me_detail_single_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        String imageUrl = mImagesList.get(position);

        holder.name_me_detail_single_item.setText(ActivityPost.get(position));
        holder.desc_me_detail_single_item.setText(ActivityMessage.get(position));
        holder.date_me_detail_single_item.setText(PostDate.get(position));
        holder.post_like_me_detail_single_item.setText(PostLike.get(position));


    }

    @Override
    public int getItemCount() {
        return ActivityPost.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name_me_detail_single_item, desc_me_detail_single_item;
        public TextView date_me_detail_single_item, post_like_me_detail_single_item;

        public MyViewHolder(View view) {
            super(view);
            name_me_detail_single_item = (TextView) view.findViewById(R.id.name_me_detail_single_item);
            desc_me_detail_single_item = (TextView) view.findViewById(R.id.desc_me_detail_single_item);
            date_me_detail_single_item = (TextView) view.findViewById(R.id.date_me_detail_single_item);
            post_like_me_detail_single_item = (TextView) view.findViewById(R.id.post_like_me_detail_single_item);
        }
    }

}