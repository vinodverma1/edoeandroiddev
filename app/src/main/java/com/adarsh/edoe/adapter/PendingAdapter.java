package com.adarsh.edoe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<String> ContactNameList, ContentPaid, UserImage;

    public PendingAdapter(Context context, ArrayList<String> NameList, ArrayList<String> ContentPaid1) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContentPaid = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.ContactNameList = NameList;
        this.ContentPaid = ContentPaid1;


    }

    @Override
    public PendingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pending_adapter, parent, false);

        return new PendingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PendingAdapter.MyViewHolder holder, final int position) {

        holder.nameTV.setText(ContactNameList.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTV, reasonTV, dateTV, amountTV;
        ImageView UserImage;
        Button cancelBtn, remindBtn;

        public MyViewHolder(View view) {
            super(view);

            reasonTV = (TextView) view.findViewById(R.id.reason_pending_adapter);
            nameTV = (TextView) view.findViewById(R.id.name_pending_adapter);
            dateTV = (TextView) view.findViewById(R.id.date_pending_adapter);
            amountTV = (TextView) view.findViewById(R.id.amount_pending_adapter);
            UserImage = (ImageView) view.findViewById(R.id.userImage_pending_adapter);
            cancelBtn = (Button) view.findViewById(R.id.cancelBtn_pending_adapter);
            remindBtn = (Button) view.findViewById(R.id.remindBtn_pending_adapter);


        }
    }
}


