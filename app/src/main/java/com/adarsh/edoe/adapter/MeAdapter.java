package com.adarsh.edoe.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.MeDetail;

import java.util.ArrayList;

public class MeAdapter extends RecyclerView.Adapter<MeAdapter.MyViewHolder> {

    private ArrayList<String> mImagesList, PostDate, PostType, MessagesCount, HeartCount, ActivityPost, ActivityMessage, Money;
    private Context mContext;

    public MeAdapter(Context context, ArrayList<String> imageList, ArrayList<String> activity, ArrayList<String> activityMessage,
                     ArrayList<String> postDate, ArrayList<String> postType, ArrayList<String> money, ArrayList<String> messageCount,
                     ArrayList<String> heartCount) {

        mImagesList = new ArrayList<String>();
        PostDate = new ArrayList<String>();
        PostType = new ArrayList<String>();
        MessagesCount = new ArrayList<String>();
        HeartCount = new ArrayList<String>();
        ActivityPost = new ArrayList<String>();
        ActivityMessage = new ArrayList<String>();
        Money = new ArrayList<String>();

        mContext = context;
        this.mImagesList = imageList;
        this.ActivityPost = activity;
        this.ActivityMessage = activityMessage;
        this.PostDate = postDate;
        this.PostType = postType;
        this.Money = money;
        this.MessagesCount = messageCount;
        this.HeartCount = heartCount;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.me_single_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        String imageUrl = mImagesList.get(position);

        holder.ActivityTV.setText(ActivityPost.get(position));
        holder.ActivityMessageTV.setText(ActivityMessage.get(position));
        holder.MessageCountTV.setText(MessagesCount.get(position));
        holder.HeartCountTV.setText(HeartCount.get(position));
        holder.PostDateTV.setText(PostDate.get(position));
        holder.PostTypeTV.setText(PostType.get(position));
        holder.MoneyTV.setText(Money.get(position));

    }

    @Override
    public int getItemCount() {
        return ActivityPost.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public ImageView imageViewTV, HeartIconTV, MessageIconTV;
        public TextView MessageCountTV, HeartCountTV, ActivityTV, ActivityMessageTV;
        public TextView PostDateTV, PostTypeTV, MoneyTV;
        LinearLayout linear_me_single_item;

        public MyViewHolder(View view) {
            super(view);

            imageViewTV = (ImageView) view.findViewById(R.id.image_me_adapter);
            HeartIconTV = (ImageView) view.findViewById(R.id.heart_me_adapter);
            MessageIconTV = (ImageView) view.findViewById(R.id.message_me_adapter);
            MessageCountTV = (TextView) view.findViewById(R.id.message_count_me_adapter);
            HeartCountTV = (TextView) view.findViewById(R.id.heart_count_me_adapter);
            ActivityTV = (TextView) view.findViewById(R.id.activity_me_adapter);
            ActivityMessageTV = (TextView) view.findViewById(R.id.activity_message_me_adapter);
            PostDateTV = (TextView) view.findViewById(R.id.date_me_adapter);
            PostTypeTV = (TextView) view.findViewById(R.id.post_type_me_adapter);
            MoneyTV = (TextView) view.findViewById(R.id.money_me_adapter);
            linear_me_single_item = (LinearLayout) view.findViewById(R.id.linear_me_single_item);
            linear_me_single_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, MeDetail.class);
                    mContext.startActivity(intent);
                }
            });

        }
    }

}