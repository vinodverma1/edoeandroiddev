package com.adarsh.edoe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class BanklistAdapter extends RecyclerView.Adapter<BanklistAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<String> ContactNameList, ContentPaid, UserImage;

    public BanklistAdapter(Context context, ArrayList<String> NameList) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContentPaid = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.ContactNameList = NameList;


    }

    @Override
    public BanklistAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.banklists_adapter, parent, false);

        return new BanklistAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BanklistAdapter.MyViewHolder holder, final int position) {

        holder.bankNameTV.setText(ContactNameList.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView bankImage;
        TextView bankNameTV;
        RelativeLayout relativeLayout;

        public MyViewHolder(View view) {
            super(view);

            bankImage = (ImageView) view.findViewById(R.id.bank_image_banklists_adapter);
            bankNameTV = (TextView) view.findViewById(R.id.bank_name_banklists_adapter);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relative_banklists_adapter);

//            relativeLayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent hhh=new Intent(mContext,ShowCard.class);
//                    mContext.startActivity(hhh);
//                }
//            });
        }
    }
}



