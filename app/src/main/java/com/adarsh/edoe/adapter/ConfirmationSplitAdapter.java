package com.adarsh.edoe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class ConfirmationSplitAdapter extends RecyclerView.Adapter<ConfirmationSplitAdapter.MyViewHolder> {

    Context mContext;
    String CHeck, Toolbar = "";
    int select = 0, SelectSplit = 0;
    ArrayList<String> ContactNameList, ContentPaid, UserImage;
    boolean MultiSelection = false;
    private SparseBooleanArray selectedItems;

    public ConfirmationSplitAdapter(Context context, ArrayList<String> NameList, ArrayList<String> ContentPaid1) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContentPaid = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.ContactNameList = NameList;
        this.ContentPaid = ContentPaid1;


    }

    @Override
    public ConfirmationSplitAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.confirmation_split_adapter, parent, false);

        return new ConfirmationSplitAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ConfirmationSplitAdapter.MyViewHolder holder, final int position) {

        holder.nameTV.setText(ContactNameList.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTV, PaidConTV;
        ImageView UserImage;

        public MyViewHolder(View view) {
            super(view);

            PaidConTV = (TextView) view.findViewById(R.id.paid_cotent_confirmationSplit_adapter);
            nameTV = (TextView) view.findViewById(R.id.name_confirmationSplit_adapter);
            UserImage = (ImageView) view.findViewById(R.id.userImage_confirmationSplit_adapter);

        }
    }
}

