package com.adarsh.edoe.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class BankAndCardAdapter2 extends RecyclerView.Adapter<BankAndCardAdapter2.MyViewHolder> {

    Context mContext;
    //public static int lastSelectedPosition = -1;
    ArrayList<String> ContactNameList, ContentPaid, UserImage;

    public BankAndCardAdapter2(Context context, ArrayList<String> NameList) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContentPaid = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.ContactNameList = NameList;


    }

    @Override
    public BankAndCardAdapter2.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.banks_and_card_adapter, parent, false);

        return new BankAndCardAdapter2.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BankAndCardAdapter2.MyViewHolder holder, final int position) {

        holder.cardBankBtn.setText(ContactNameList.get(position).toString());

        try {
            //since only one radio button is allowed to be selected,
            // this condition un-checks previous selections
            holder.radioButton.setChecked(BanksAndCardAdapter.lastSelectedPosition == position);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        Button cardBankBtn;
        RadioGroup radioGroup;
        RadioButton radioButton;

        public MyViewHolder(View view) {
            super(view);

            cardBankBtn = (Button) view.findViewById(R.id.payBtn_banks_and_card_adapter);
            radioButton = (RadioButton) view.findViewById(R.id.radioBtn_banks_and_card_adapter);

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BanksAndCardAdapter.lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                    try {
                        Toast.makeText(mContext,
                                ContactNameList.get(BanksAndCardAdapter.lastSelectedPosition).toString(),
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}



