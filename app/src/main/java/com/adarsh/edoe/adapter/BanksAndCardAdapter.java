package com.adarsh.edoe.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.ShowCard;

import java.util.ArrayList;

public class BanksAndCardAdapter extends RecyclerView.Adapter<BanksAndCardAdapter.MyViewHolder> {

    public static int lastSelectedPosition = -1;
    Context mContext;
    ArrayList<String> ContactNameList, ContentPaid, UserImage;

    public BanksAndCardAdapter(Context context, ArrayList<String> NameList) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContentPaid = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.ContactNameList = NameList;


    }

    @Override
    public BanksAndCardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.banks_and_card_adapter, parent, false);

        return new BanksAndCardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BanksAndCardAdapter.MyViewHolder holder, final int position) {

        try {

            //since only one radio button is allowed to be selected,
            // this condition un-checks previous selections
            holder.radioButton.setChecked(lastSelectedPosition == position);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.cardBankBtn.setText("BANK CARD NAME" + ContactNameList.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        Button cardBankBtn;
        RadioButton radioButton;
        RelativeLayout relativeLayout;

        public MyViewHolder(View view) {
            super(view);

            cardBankBtn = (Button) view.findViewById(R.id.payBtn_banks_and_card_adapter);
            radioButton = (RadioButton) view.findViewById(R.id.radioBtn_banks_and_card_adapter);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relative_banks_and_card_adapter);

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                    try {
                        Toast.makeText(mContext,
                                ContactNameList.get(lastSelectedPosition).toString(),
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent hhh = new Intent(mContext, ShowCard.class);
                    mContext.startActivity(hhh);
                }
            });

            cardBankBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent hhh = new Intent(mContext, ShowCard.class);
                    mContext.startActivity(hhh);
                }
            });
        }
    }
}


