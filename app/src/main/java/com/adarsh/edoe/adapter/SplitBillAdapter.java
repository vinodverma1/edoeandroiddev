package com.adarsh.edoe.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.CreateBill;
import com.adarsh.edoe.activity.SplitBill;

import java.util.ArrayList;

/**
 * Created by Adarsh on 13-04-2018.
 */

public class SplitBillAdapter extends RecyclerView.Adapter<SplitBillAdapter.MyViewHolder> {

    Context mContext;
    String CHeck, Toolbar = "", Amount = "";
    int select = 0, SelectSplit = 0;
    ArrayList<String> ContactNameList, ContactNumberList;
    ArrayList<String> selectedNameList = new ArrayList<>();
    ArrayList<String> selectedNumberList = new ArrayList<>();
    ArrayList<String> clickedSplitList = new ArrayList<>();
    boolean MultiSelection = false;
    private SparseBooleanArray selectedItems;

    public SplitBillAdapter(Context context, ArrayList<String> NameList, ArrayList<String> NumberList, String check,
                            String Toolbar1, String Amount1) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContactNumberList = new ArrayList<String>();

        this.ContactNameList = NameList;
        this.ContactNumberList = NumberList;
        this.CHeck = check;
        this.Toolbar = Toolbar1;
        this.Amount = Amount1;
    }

    @Override
    public SplitBillAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contactlist_adapter, parent, false);

        return new SplitBillAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SplitBillAdapter.MyViewHolder holder, final int position) {

        holder.nameTV.setText(ContactNameList.get(position).toString());

        ((SplitBillAdapter.MyViewHolder) holder).linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickedSplitList.contains(String.valueOf(position))) {
                    try {
                        clickedSplitList.remove(String.valueOf(position));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        selectedNameList.remove(ContactNameList.get(position).toString());
                        selectedNumberList.remove(ContactNumberList.get(position).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    holder.UserImage.setVisibility(View.VISIBLE);
                    holder.tick.setVisibility(View.GONE);
                } else {
                    clickedSplitList.add(String.valueOf(position));
                    select = 1;

                    selectedNameList.add(ContactNameList.get(position).toString());
                    selectedNumberList.add(ContactNumberList.get(position).toString());

                    holder.UserImage.setVisibility(View.GONE);
                    holder.tick.setVisibility(View.VISIBLE);

                }

                if (clickedSplitList.size() > 0) {
                    SplitBill.NextBtn.setBackgroundColor(mContext.getResources().getColor(R.color.ThemeColor));
                } else {
                    Toast.makeText(mContext, "Please Select Any Contact", Toast.LENGTH_SHORT).show();
                    SplitBill.NextBtn.setBackgroundColor(mContext.getResources().getColor(R.color.grey));
                }

            }
        });

        SplitBill.NextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickedSplitList.size() > 0) {
                    Intent next = new Intent(mContext, CreateBill.class);
                    next.putExtra("Amount", Amount);
                    next.putExtra("selectedNameList", selectedNameList);
                    next.putExtra("selectedNumberList", selectedNumberList);
                    mContext.startActivity(next);
                } else {
                    Toast.makeText(mContext, "Please Select Contacts", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV;
        ImageView tick, UserImage;
        LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);

            linearLayout = (LinearLayout) view.findViewById(R.id._contact_adapter);
            nameTV = (TextView) view.findViewById(R.id.contactName_contact_adapter);
            tick = (ImageView) view.findViewById(R.id.tick_contact_adapter);
            UserImage = (ImageView) view.findViewById(R.id.contactImage_contact_adapter);

        }
    }
}
