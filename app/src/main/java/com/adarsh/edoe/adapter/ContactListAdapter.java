package com.adarsh.edoe.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.ContactInfo;
import com.adarsh.edoe.activity.PayActionPerform;
import com.adarsh.edoe.activity.SendTo;

import java.util.ArrayList;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.MyViewHolder> {

    Context mContext;
    String CHeck, Toolbar = "";
    String Amount = "";
    int select = 0, SelectSplit = 0;
    ArrayList<String> ContactNameList, ContactNumberList;
    ArrayList<Integer> clickedList = new ArrayList<>();
    ArrayList<Integer> clickedSplitList = new ArrayList<>();
    boolean MultiSelection = false;
    private SparseBooleanArray selectedItems;

    public ContactListAdapter(Context context, ArrayList<String> NameList, ArrayList<String> NumberList, String check,
                              String Toolbar1, String Amount1) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContactNumberList = new ArrayList<String>();

        this.ContactNameList = NameList;
        this.ContactNumberList = NumberList;
        this.CHeck = check;
        this.Toolbar = Toolbar1;
        this.Amount = Amount1;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contactlist_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.nameTV.setText(ContactNameList.get(position).toString());

        ((MyViewHolder) holder).linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = 0;

                if (CHeck.equalsIgnoreCase("From Action")) {

                    if (MultiSelection) {
                        if (clickedList.contains(position)) {
                            select = 0;
                            clickedList.clear();
                            MultiSelection = false;
                            holder.UserImage.setVisibility(View.VISIBLE);
                            holder.tick.setVisibility(View.GONE);
                        }
                    } else {
                        MultiSelection = true;
                        if (clickedList.contains(position)) {
                            select = 0;
                            MultiSelection = false;
                            holder.UserImage.setVisibility(View.VISIBLE);
                            holder.tick.setVisibility(View.GONE);
                        } else {
                            clickedList.add(position);
                            select = 1;

                            holder.UserImage.setVisibility(View.GONE);
                            holder.tick.setVisibility(View.VISIBLE);
                        }
                    }
                    if (clickedList.size() > 0) {
                        PayActionPerform.NextBtn.setBackgroundColor(mContext.getResources().getColor(R.color.ThemeColor));
                    } else {
                        Toast.makeText(mContext, "Please Select Any Contact", Toast.LENGTH_SHORT).show();
                        PayActionPerform.NextBtn.setBackgroundColor(mContext.getResources().getColor(R.color.grey));
                    }
                } else if (CHeck.equalsIgnoreCase("Split")) {

                    if (clickedSplitList.contains(position)) {
                        try {
                            clickedSplitList.remove(position);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        holder.UserImage.setVisibility(View.GONE);
                        holder.tick.setVisibility(View.VISIBLE);
                    } else {
                        clickedSplitList.add(position);
                        select = 1;

                        holder.UserImage.setVisibility(View.GONE);
                        holder.tick.setVisibility(View.VISIBLE);

                    }


                } else {
                    Toast.makeText(mContext, ContactNameList.get(position).toString(), Toast.LENGTH_SHORT).show();
                    Intent next = new Intent(mContext, ContactInfo.class);
                    next.putExtra("ContactName", ContactNameList.get(position).toString());
                    next.putExtra("ContactNumber", ContactNumberList.get(position).toString());
                    mContext.startActivity(next);
                }
            }
        });

        try {
            PayActionPerform.NextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (clickedList.size() > 0) {
                        try {
                            int poo = Toolbar.indexOf(" ");
                            Toolbar = Toolbar.substring(0, poo);
                        } catch (Exception e) {
                            Toolbar = "";
                            e.printStackTrace();
                        }
                        Intent next = new Intent(mContext, SendTo.class);
                        next.putExtra("Toolbar", Toolbar);
                        next.putExtra("Amount", Amount);
                        mContext.startActivity(next);
                    } else {
                        Toast.makeText(mContext, "Please Select Any Contact", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV;
        ImageView tick, UserImage;
        LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);

            linearLayout = (LinearLayout) view.findViewById(R.id._contact_adapter);
            nameTV = (TextView) view.findViewById(R.id.contactName_contact_adapter);
            tick = (ImageView) view.findViewById(R.id.tick_contact_adapter);
            UserImage = (ImageView) view.findViewById(R.id.contactImage_contact_adapter);

        }
    }
}