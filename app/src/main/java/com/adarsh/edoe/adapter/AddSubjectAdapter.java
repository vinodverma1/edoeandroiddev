package com.adarsh.edoe.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class AddSubjectAdapter extends RecyclerView.Adapter<AddSubjectAdapter.MyViewHolder> {

    private ArrayList<String> mImagesList, mNameList;
    private Context mContext;

    public AddSubjectAdapter(Context context, ArrayList<String> imageList, ArrayList<String> namelist) {

        mImagesList = new ArrayList<String>();
        mContext = context;
        this.mImagesList = imageList;
        this.mNameList = namelist;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_subject_single_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        String imageUrl = mImagesList.get(position);

        holder.name_add_subject_single_item.setText(mNameList.get(position));
    }

    @Override
    public int getItemCount() {
        return mNameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name_add_subject_single_item;


        public MyViewHolder(View view) {
            super(view);
            name_add_subject_single_item = (TextView) view.findViewById(R.id.name_add_subject_single_item);
        }
    }

}