package com.adarsh.edoe.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adarsh.edoe.R;
import com.adarsh.edoe.activity.ChatToContact;

import java.util.ArrayList;

public class AddToChatAdapter extends RecyclerView.Adapter<AddToChatAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<String> ContactNameList, ContactNumberList;
    ArrayList<Drawable> UserImage;

    public AddToChatAdapter(Context context, ArrayList<String> NameList, ArrayList<String> ContactNumberList1,
                            ArrayList<Drawable> ImageList1) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContactNumberList = new ArrayList<String>();
        UserImage = new ArrayList<>();

        this.ContactNameList = NameList;
        this.ContactNumberList = ContactNumberList1;
        this.UserImage = ImageList1;
    }

    @Override
    public AddToChatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contactlist_adapter, parent, false);

        return new AddToChatAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AddToChatAdapter.MyViewHolder holder, final int position) {

        holder.nameTV.setText(ContactNameList.get(position).toString());
        holder.UserImage.setImageDrawable(UserImage.get(position));

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chat = new Intent(mContext, ChatToContact.class);
                chat.putExtra("ContactName", ContactNameList.get(position).toString());
                mContext.startActivity(chat);
            }
        });

    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTV;
        ImageView tick, UserImage;
        LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);

            linearLayout = (LinearLayout) view.findViewById(R.id._contact_adapter);
            nameTV = (TextView) view.findViewById(R.id.contactName_contact_adapter);
            tick = (ImageView) view.findViewById(R.id.tick_contact_adapter);
            UserImage = (ImageView) view.findViewById(R.id.contactImage_contact_adapter);


        }
    }
}



