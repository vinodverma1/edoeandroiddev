package com.adarsh.edoe.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.adarsh.edoe.R;

import java.util.ArrayList;

public class CreateBillAdapter extends RecyclerView.Adapter<CreateBillAdapter.MyViewHolder> {

    Context mContext;
    String CHeck, Toolbar = "";
    int select = 0, SelectSplit = 0;
    ArrayList<String> ContactNameList, ContactNumberList;
    ArrayList<String> selectedNameList = new ArrayList<>();
    ArrayList<String> selectedNumberList = new ArrayList<>();
    ArrayList<String> clickedSplitList = new ArrayList<>();
    boolean MultiSelection = false;
    private SparseBooleanArray selectedItems;

    public CreateBillAdapter(Context context, ArrayList<String> NameList, ArrayList<String> NumberList) {
        mContext = context;


        ContactNameList = new ArrayList<String>();
        ContactNumberList = new ArrayList<String>();

        this.ContactNameList = NameList;
        this.ContactNumberList = NumberList;


    }

    @Override
    public CreateBillAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.create_bill_adapter, parent, false);

        return new CreateBillAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CreateBillAdapter.MyViewHolder holder, final int position) {


        holder.crossBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactNameList.remove(position);
                ContactNumberList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return ContactNameList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView UserImage;
        ImageButton crossBtn;

        public MyViewHolder(View view) {
            super(view);

            crossBtn = (ImageButton) view.findViewById(R.id.crossBtn_createBill_adapter);
            UserImage = (ImageView) view.findViewById(R.id.userImage_createBill_adapter);

        }
    }
}
